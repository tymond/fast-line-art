#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#

import torch
import getopt
import sys
import csv
import re

from torchvision import transforms
from PIL import Image
from torchvision.utils import save_image
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import numpy
import random

from modelsDefinitions import *
from taskfileParser import *


def printHelp():
	print(f"Options:")
	print(f"            -h, --help              show this help text")
	print(f"            -m, --modelType=TYPE    type of model to show the summary of")
	print(f"            -w, --width=WIDTH       width of the input image")
	print(f"            -t, --height=HEIGHT     height of the input image")
	




if __name__ == "__main__":
	

	statisticsFile = ""
	which = ""
	range = ""


	try:
		arguments, values = getopt.getopt(sys.argv[1:], "hs:w:r:", ["help", "stats=", "which=", "range="])
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-h", "--help"):
				printHelp()
				quit()
				
			elif currentArgument in ("-s", "--stats"):
				statisticsFile = currentValue
			
			elif currentArgument in ("-w", "--which"):
				which = currentValue
			
			elif currentArgument in ("-r", "--range"):
				range = currentValue
			
	except getopt.error as err:
		showErrorAndQuit(str(err))

	rangeStart = -1
	rangeEnd = -1
	if range != "":
		regex = re.compile(r"\d+")
		match = regex.findall(range)
		if match == None or len(match) != 2:
			showErrorAndQuit("The range argument is incorrect.")
		rangeStart = int(match[0])
		rangeEnd = int(match[1])
		
		
	


	if (statisticsFile == ""):
		showErrorAndQuit("Please provide the statistics file.")
	
	epochs = []
	trainingLoss = []
	verifyLoss = []

	with open(statisticsFile, newline='') as stats:
		r = csv.reader(stats, delimiter=";")
		for i, row in enumerate(r):
			if len(row) > 0 and row[0] != "Epoch" and int(row[0]) != i - 1:
				showErrorAndQuit(f"Something went wrong in the statistics file. Please fix it manually and run the training again. i={i}, row[0] = {row[0]}")
			
			if row[0] == "Epoch":
				continue
			epochs.append(int(row[0]))
			trainingLoss.append(float(row[1]))
			verifyLoss.append(float(row[2]))
	x = y = numpy.array([])

	if rangeStart != -1:
		rangeEnd = min(rangeEnd, len(trainingLoss))
		plt.plot(epochs[rangeStart:rangeEnd], trainingLoss[rangeStart:rangeEnd])
		plt.plot(epochs[rangeStart:rangeEnd], verifyLoss[rangeStart:rangeEnd])
		x = numpy.array(epochs[rangeStart:rangeEnd]).reshape((-1, 1))
		y = numpy.array(verifyLoss[rangeStart:rangeEnd])		

	else:
		plt.plot(epochs, trainingLoss)
		plt.plot(epochs, verifyLoss)
		x = numpy.array(epochs).reshape((-1, 1))
		y = numpy.array(verifyLoss)
	
	normalizeEpochs = 10000
	x = x/normalizeEpochs
	lrmodel = LinearRegression()

	print(f"shapes: {x.shape} {y.shape}")



	#x = numpy.random.uniform(0.008, 0.009, (x.shape))
	#y = numpy.random.uniform(0.008, 0.009, (y.shape))


	#x = numpy.array([1, 2, 3, 4, 5, 6, 7, 8, 9]).reshape((-1, 1))
	#y = numpy.random.uniform(1.0, 2.0, (9,))

	#print(f"y = {y}")
	

	print(f"shapes: {x.shape} {y.shape}")
	lrmodel.fit(x, y)
	prediction = lrmodel.predict(x)

	r_sq = lrmodel.score(x, y)
	print(f"coefficient of determination: {r_sq}")
	print(f"intercept: {lrmodel.intercept_}")
	print(f"slope: {lrmodel.coef_}")


	plt.plot(x*normalizeEpochs, prediction)



	plt.show()

	






