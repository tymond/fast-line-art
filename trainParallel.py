#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

import torch
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader

import torch.multiprocessing as mp

from torch.nn.parallel import DistributedDataParallel as DDP
from torch.distributed import init_process_group, destroy_process_group
import os
import sys

import datetime



from trainerParallel import Trainer
from weightedDistributedSampler import WeightedDistributedSampler
from devicesProcessesInfo import DevicesProcessesInfo


from imageDataset import *
from taskfileParser import *
import metaHandler
import signal
import traceback
import datetime


def ddp_setup(rank, world_size, ethernetInterface, port):
	"""
	Args:
		rank: Unique identifier of each process
		world_size: Total number of processes
	"""
	os.environ["MASTER_ADDR"] = "localhost"
	os.environ["MASTER_PORT"] = port
	os.environ["GLOO_SOCKET_IFNAME"] = ethernetInterface
	#torch.cuda.set_device(rank)
	init_process_group(backend="gloo", rank=rank, world_size=world_size, timeout=datetime.timedelta(minutes = 10)) # we want it to wait (even if a while), not crash


class MyTrainDataset(Dataset):
    def __init__(self, size):
        self.size = size
        #self.data = [(torch.rand(20), torch.rand(1)) for _ in range(size)]
        #tmp = [torch.rand(1) for _ in range(size)]
        tmp = [float(i)/size for i in range(size)]
        
        self.data = [(float(tmp[i]/4), float(tmp[i])) for i in range(size)]

    def __len__(self):
        return self.size
    
    def __getitem__(self, index):
        return self.data[index]


def load_train_objs_demo(datasetSize=10000, optimizer=torch.optim.SGD, learningRate=1e-3):
	train_set = MyTrainDataset(datasetSize) # load your dataset
	model = torch.nn.Sequential(
		torch.nn.Linear(1, 2, dtype=torch.float32),
		torch.nn.Linear(2, 1, dtype=torch.float32)
	)
	#torch.nn.Linear(20, 1)  # load your model
	optimizer = optimizer(model.parameters(), lr=learningRate)
	return train_set, model, optimizer

def loadTrainObjects(task: TaskInformation, device):
	trainSet = ImageDatasetOnDemand(task.pathsOptions.getTrainingSetFullPath())
	validationSet = ImageDatasetOnDemand(task.pathsOptions.getValidationSetFullPath())
	
	model =  utilities.getModelFromFileOrNew(task.trainingOptions.modelType, task.pathsOptions.getMainModelFileFullPath(), device)
	optimizer = utilities.getOptimizer(task.trainingOptions.optimizer, model, task.trainingOptions.learningRate)

	return (trainSet, validationSet, model, optimizer)


def prepare_dataloader(dataset: Dataset, batch_size: int, start_data_rank: int, volume: int, num_replicas: int): # must be AFTER init_process_group
	
	sampler = WeightedDistributedSampler(dataset, start_data_rank, volume, num_replicas)
	print(f"num replicas = {num_replicas}")
	return DataLoader(
		dataset,
		batch_size=batch_size,
		pin_memory=True,
		shuffle=False,
		sampler=sampler
	)


def main(rank: int, taskfilePath: str, world_size: int, total_epochs: int, batch_size: int, cpuArg, cudaArg, interface: str, port: str):


	rankRangesInfo = DevicesProcessesInfo()
	rankRangesInfo.fromArgs(cpuArg, cudaArg)
	device = rankRangesInfo.devicePerRank(rank)
	if device == "cuda":
		print(f"{datetime.datetime.now()} | CUDA process in worker {rank}", sys.stderr)


	task : TaskInformation = TaskFileParser().parseFile(taskfilePath)
	if not task.checkIntegrity():	
		showErrorAndQuit("Task file cannot be parsed.")

	print(f"DDp setup ({rank}):")
	ddp_setup(rank, world_size, interface, port) # includes init_process_group

	save_every = task.trainingOptions.savingModelBackupFrequency
	assert save_every == 1, "save_every/savingModelBackupFrequency must be equal to 1 in this case due to signal handlers"
	startEpoch = 0
	if rank == 0:
		startEpoch = metaHandler.startTraining(task, (rank != 0))
	
	startEpochTensor = torch.tensor(startEpoch)
	print(f"Tensor is now [{rank}]: [{startEpochTensor}]")
	torch.distributed.broadcast(startEpochTensor, 0)
	print(f"Tensor is now 2 [{rank}]: [{startEpochTensor}]")
	
	startEpoch = startEpochTensor

	
	print(f"Loading stuff ({rank}):")
	trainDataset, validationDataset, model, optimizer = loadTrainObjects(task, device)
	
	#print(f"### Beginning model for ({rank}) looks like this : {model.state_dict()}")
	print(f"Loading train data ({rank}):")


	train_data = prepare_dataloader(trainDataset, batch_size, rankRangesInfo.startDataRank(rank), rankRangesInfo.volumePerRank(rank), rankRangesInfo.volumesSum())
	val_data = prepare_dataloader(validationDataset, batch_size, rankRangesInfo.startDataRank(rank), rankRangesInfo.volumePerRank(rank), rankRangesInfo.volumesSum())
	
	print(f"Creating trainer ({rank} => {device} because {rankRangesInfo.processesSum()}):")
	
	trainer = Trainer(model, task, train_data, val_data, optimizer, rank, device, save_every, rankRangesInfo)
	#print(f"### Model for ({rank}) after creating trainer looks like this : {model.state_dict()}")
	print(f"Training now ({rank}):")
	try:
		trainer.train(startEpoch, total_epochs)
	except Exception as e:
		print(f"[worker = {rank}] Exception in the thread: {e} (type: {type(e)})", file=sys.stderr)
		print(f"[worker = {rank}] Exception in the thread (type): {type(e).__name__}", file=sys.stderr)
		#traceback.print_exc(file=sys.stderr)
		exceptionStr = traceback.format_exc().split('\n')
		for line in exceptionStr:
			print(f"[worker = {rank}] {line}", file=sys.stderr)
		#print(f"[worker = {rank}] [{exceptionStr}]", file=sys.stderr)
		
		print(f"[worker = {rank}] Exception in the thread (last) ^", file=sys.stderr)

	if rank == 0:
		metaHandler.endTrainingMetaGuessEpoch(task)

	print(f"Destroy process group ({rank}):")
	destroy_process_group()
	print(f"Done ({rank})")




if __name__ == "__main__":
	print(f"~~~~~~~~~~~~~~~~~~~~~ START ~~~~~~~~~~~~~~~~~~~~~", file=sys.stderr)
	import argparse
	parser = argparse.ArgumentParser(description='Train in parallel')
	parser.add_argument('-c', '--cpu', type=str, help="Tuple of (processesCount, volume) for 'cpu' device")
	parser.add_argument('-g', '--cuda', type=str, help="Tuple of (processesCount, volume) for 'cuda' device (cuda:0)")
	parser.add_argument('-t', '--taskfile', type=str, help="Path to taskfile")
	

	parser.add_argument("-e", '--total_epochs', type=int, help='Total epochs to train the model')
	parser.add_argument("-i", '--interface', type=str, help='Ethernet interface (i.e. eth0)')
	parser.add_argument("-p", '--port', type=str, help='Port to use (i.e. 12356)')
	
	
	args = parser.parse_args()

	rankRangesInfo = DevicesProcessesInfo()
	rankRangesInfo.fromArgs(args.cpu, args.cuda)

	def sigintHandler(signum, _frame):
		
		signal.signal(signum, signal.SIG_IGN) # ignore future signals
		print(f"We (main thread) got signal to finish work: {signum}", file=sys.stderr)
		task : TaskInformation = TaskFileParser().parseFile(taskfilePath)
		metaHandler.endTrainingMetaGuessEpoch(task)
		sys.exit(0)
	
	signal.signal(signal.SIGINT, sigintHandler)
	signal.signal(signal.SIGHUP, sigintHandler)
	signal.signal(signal.SIGTERM, sigintHandler)
	
	
	#parser.add_argument('save_every', type=int, help='How often to save a snapshot')
	#parser.add_argument('--batch_size', default=32, type=int, help='Input batch size on each device (default: 32)')
	
	total_epochs = 10 if args.total_epochs is None else args.total_epochs
	interface = "eno1" if (args.interface is None) else args.interface
	port = "12356" if (args.port is None) else args.port
	batch_size = 1
	#save_every = 1

	world_size = rankRangesInfo.processesSum()

	for i in range(world_size):
		print(f"start data rank for {i} = {rankRangesInfo.startDataRank(i)}")

	taskfilePath = args.taskfile

	print("Spawning main!")
	mp.spawn(main, args=(taskfilePath, world_size, total_epochs, batch_size, args.cpu, args.cuda, interface, port), nprocs=world_size)
	print("And that's the end of it.")

	print(f"~~~~ END OF PARALLEL, successful!!! ~~~~")
	print(f"~~~~ END OF PARALLEL, successful!!! ~~~~", file=sys.stderr)
	print(f"Devices per rank: {rankRangesInfo.devicePerRank(0)}, {rankRangesInfo.devicePerRank(1)}, {rankRangesInfo.devicePerRank(2)}")
