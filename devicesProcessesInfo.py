#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#




from ast import literal_eval as make_tuple


class DevicesProcessesInfo:

	class RankRangeInfo:
		def __init__(self, processesCount, device, volume):
			self.processesCount = processesCount
			self.device = device
			self.volume = volume

		volume : int = 0
		device : str = ""
		processesCount : int = 0
	
	rankRanges = []

	def __init__(self) -> None:
		pass
		#self.rankRanges = [self.RankRangeInfo(2, "cpu", 1), self.RankRangeInfo(1, "cuda", 2)]

	def fromArgs(self, cpuArg, cudaArg):
		if cpuArg is not None:
			tup = make_tuple(cpuArg)
			if tup is not None and len(tup) >= 2:
				self.addRange(tup[0], "cpu", tup[1])
				

		if cudaArg is not None:
			tup = make_tuple(cudaArg)
			if tup is not None and len(tup) >= 2:
				self.addRange(tup[0], "cuda", tup[1])
		
		if self.processesSum() == 0:
			self.addRange(1, "cpu", 1)


	def addRange(self, processesCount : int, device : str, volume : int):
		self.rankRanges.append(self.RankRangeInfo(processesCount, device, volume))


	def processesSum(self) -> int:
		return sum([rankInfo.processesCount for rankInfo in self.rankRanges])

	def volumesSum(self) -> int:
		return sum([rankInfo.volume*rankInfo.processesCount for rankInfo in self.rankRanges])

	def _findRankInfo(self, rank) -> RankRangeInfo:
		suma = 0
		for rankInfo in self.rankRanges:
			if rank >= suma and rank < suma + rankInfo.processesCount:
				return rankInfo
			suma += rankInfo.processesCount
		return None

	def devicePerRank(self, rank):
		rankInfo = self._findRankInfo(rank)
		if rankInfo is None:
			return "cpu"
		return rankInfo.device

	def volumePerRank(self, rank):
		rankInfo = self._findRankInfo(rank)
		if rankInfo is None:
			return 1
		return rankInfo.volume

	def startDataRank(self, rank):
		dataRankSum = 0
		rankSum = 0
		for rankInfo in self.rankRanges:
			
			beforeRank = rankSum
			afterRank = rankSum + rankInfo.processesCount

			beforeDataRank = dataRankSum
			afterDataRank = dataRankSum + rankInfo.processesCount*rankInfo.volume
			
			if rank >= rankSum and rank < rankSum + rankInfo.processesCount:
				# this is the process
				# but exact data rank depends on the index in the range, too
				indexInRange = rank - rankSum
				return beforeDataRank + indexInRange*rankInfo.volume
			
			rankSum = afterRank
			dataRankSum = afterDataRank



