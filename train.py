#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#

#import torch
import time
import numpy as np
import matplotlib.pyplot as plt
import datetime
import getopt
import utilities as utils
import sys
import signal

from os import listdir
from os.path import isfile, join
from sklearn.utils import shuffle



from trainer import *
from taskfileParser import *
from metaHandler import *

import argparse



def printHelp():
	print(f"Options:")
	print(f"            -h, --help                          show this help text")
	print(f"            -t, --taskfile=PATH                 location of the task file, containing all information needed for the training")
	print(f"            -d, --device=DEVICE                 preferred device")
	# TODO: describe the data structure after it's crystallized



def getTaskFileFromCommandlineArgs():
	
	taskfile = None
	preferredDevice = "cuda"
	try:
		arguments, values = getopt.getopt(sys.argv[1:], "hd:t:", ["help", "taskfile=", "device="])
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-h", "--help"):
				printHelp()
				quit()

			elif currentArgument in ("-t", "--taskfile"):
				taskfile = currentValue

			elif currentArgument in ("-d", "--device"):
				preferredDevice = currentValue
				print(f"Setting preferred device to: {currentValue} {preferredDevice}")
	
	except getopt.error as err:
		showErrorAndQuit(str(err))
		
	return (taskfile, preferredDevice)


def getArgParser():
	parser = argparse.ArgumentParser(description='Train models')
	parser.add_argument('-d', '--device', type=str, help="Preferred device")
	parser.add_argument('-t', '--taskfile', type=str, help="Path to taskfile")
	

	parser.add_argument("-f", '--full_verify', help='Use full verify', action='store_true')
	return parser


if __name__ == "__main__":
	
	utils.eprint("Starting training at: " + datetime.datetime.now().strftime("%d %B %Y %H:%M:%S"))

	
	parser = getArgParser()
	args = parser.parse_args()

	
	#(taskfile, preferredDevice) = getTaskFileFromCommandlineArgs()
	taskfile = args.taskfile
	preferredDevice = "cuda" if args.device is None else args.device


	device = utils.getDevice("cuda", preferredDevice)
	print(f"The selected device is: {device}", file=sys.stderr)


	task : TaskInformation = TaskFileParser().parseFile(taskfile)
	if not task.checkIntegrity():	
		showErrorAndQuit("Task file cannot be parsed.")

	if (device == "cpu"):
		task.trainingOptions.maxTrainingImageSize = 1024
	elif (device == "cuda"):
		task.trainingOptions.maxTrainingImageSize = 512
	

	ignoreCrash = True

	startEpoch = startTraining(task, dry=False, ignoreCrash=True)
	

	(trainSketches, trainInks) = utilities.getImageFilepaths(task.pathsOptions.getTrainingSetFullPath())
	(trainSketches, trainInks) = (np.array(trainSketches), np.array(trainInks))
	(verifySketches, verifyInks) = utilities.getImageFilepaths(task.pathsOptions.getValidationSetFullPath())
	
	
	model = utilities.getModelFromFileOrNew(task.trainingOptions.modelType, task.pathsOptions.getMainModelFileFullPath(), device)
	tr = Trainer(model, task)

	currentEpoch = startEpoch

	utils.eprint("Starting training at: " + datetime.datetime.now().strftime("%d %B %Y %H:%M:%S"))
	
	
	def sigintHandler(signum, _frame):
		
		signal.signal(signum, signal.SIG_IGN) # ignore future signals
		print(f"We got signal to finish work: {signum}", file=sys.stderr)
		endTrainingMeta(task, currentEpoch)
		sys.exit(0)
	
	signal.signal(signal.SIGINT, sigintHandler)
	signal.signal(signal.SIGHUP, sigintHandler)
	signal.signal(signal.SIGTERM, sigintHandler)
	
	


	start = time.time()


	# NOTE: 0.00845 is the error of just blurrying the input
	# so we should get way lower
	# smallConv = 300/h for 50 images, 5 verify
	# fake on wide & shallow: 400*10 = 4h
	# epochs = 400*25 #400*16 # 300*8

	
	########################
	

	losses = []
	trlosses = []

	last_loss = 0
	last_trloss = 0

	epochs = 10000
	maxImagesCountInEpoch = 100
	maxImagesCountInVerify = 30
	# TODO: make it into a taskfile option!
	#randomVerify = False
	#randomEpoch = True

	fullVerify = False if (args.full_verify is None) else args.full_verify

	verifyLength = len(verifySketches) if fullVerify else min(len(verifySketches), maxImagesCountInVerify)
	randomVerify = False if fullVerify else True

	verifySketchesLocal = verifySketches[0:verifyLength]
	verifyInksLocal = verifyInks[0:verifyLength]
	
	
	

	for i in range(startEpoch, startEpoch + epochs):
		print("Epoch {}".format(i))
		epochStart = time.time()

		currentEpoch = i
		
		model.train(True)
		
		(trainSketches, trainInks) = shuffle(trainSketches, trainInks)
		length = min(len(trainSketches), maxImagesCountInEpoch)
		#print(f"Length of train Sketches = {length}, first few pictures: {trainSketches[0:(min(length, 3))]}, {trainInks[0:(min(length, 3))]}", file=sys.stderr)
		trainSketchesLocal = trainSketches[0:length]
		trainInksLocal = trainInks[0:length]

		trloss = tr.trainOneEpoch(model, device, trainSketchesLocal, trainInksLocal, i)
		
		trlosses.append(trloss)
		last_trloss = trloss
		
		model.train(False)
		
		(verifySketches, verifyInks) = shuffle(verifySketches, verifyInks)
		if randomVerify:
			verifySketchesLocal = verifySketches[0:verifyLength]
			verifyInksLocal = verifyInks[0:verifyLength]


		loss = tr.verifyOneEpoch(model, device, verifySketchesLocal, verifyInksLocal, i)
		
		
		losses.append(loss)
		last_loss = loss
		
		updateTrainingMeta(task, i, trloss, loss, model)
		
		epochEnd = time.time()
		print(f"Epoch {currentEpoch} (took {epochEnd - epochStart}) => train loss = {trloss}, verify loss = {loss}")
	
	
	endTrainingMeta(task, startEpoch + epochs)
	
	end = time.time()
	print("Training took {}s".format(end - start))
	print("Ending training at: " + datetime.datetime.now().strftime("%d %B %Y %H:%M:%S"))


	torch.save(model, task.pathsOptions.getMainModelFileFullPath())


	plt.plot(losses)
	plt.plot(trlosses)
	plt.show()




