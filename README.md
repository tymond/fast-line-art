## Introduction

There are four functional scripts here:

- `dataPreparer.py`
- `train.py`
- `inkit.py`
- `summary.py`


All of them support `--help` and `-h` options to show you all the options available.

## Data augmentation

This script takes all the files in `RAW_DATA_LOCATION`, does all the data augmentation processes on them, places the output files in
`PROCESSED_DATA_LOCATION`, then divides them into a training set and a test set and places them in `DIVIDED_DATA_LOCATION`.

The files in `RAW_DATA_LOCATION` should follow the pattern of all lineart/inked pictures being called `ink_AAAA.png` while the corresponding sketch should be called
`sketch_AAAA.png`.

In the future, the API of this script will change significantly. It will work based on a task file like the main training script, and it will divide the files into
training and test sets based on a file containing two lists of filenames to ensure deterministic division in all sorts of circumstances.

    ./dataPreparer.py --raw=RAW_DATA_LOCATION 
         --processed=PROCESSED_DATA_LOCATION --divided=DIVIDED_DATA_LOCATION
    

## Training


Start a new training using this command:

    ./train.py --create --inputModelType=MODEL_TYPE --outputModelPath=OUTPUT_MODEL_PATH --data=DIVIDED_DATA_PATH

Continue training using this command:

    ./train.py --inputModelPath=INPUT_MODEL_PATH --outputModelPath=OUTPUT_MODEL_PATH --data=DIVIDED_DATA_PATH


Note that this script and its API will too change significantly in the future.

As `MODEL_TYPE` you can use "tooSmallConv". Models and their names will change as well.


## Inferencing

    ./inkit.py --modelFile=MODEL_PATH --input=INPUT_FILE_PATH --output=OUTPUT_FILE_PATH

This script will inference the file using the model from the file and is unlikely to change significantly in the future.

## Other

Script `summary.py` is just a tiny utility script that allows you to see the number of parameters in the model and the amount of memory needed for processing a specific size of an image.

    ./inkit.py --modeltype=MODEL_TYPE --width=INPUT_FILE_WIDTH --height=INPUT_FILE_HEIGHT

