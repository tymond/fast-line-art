#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#



from utilities import *
import sys
import re

from os import listdir
from os.path import isfile, join, isdir
from pathlib import Path


def shouldRemoveModel(id: int):
	leaveEvery = 10
	if (id <= 100):
		return False
	if (id%leaveEvery == 0):
		return False
	if ((id + 1)%leaveEvery == 0):
		return False
	return True




if __name__ == "__main__":


	args = handleCommandlineOptions(sys.argv[1:], [("path", "p", "1", "path to the folder with models to remove")])



	if args["path"] is not None:
		path = args["path"]
	else:
		eprint(f"ERROR: provide a path to the folder with models to remove")

	# TODO: maybe different options for stripping the folder


	regex = re.compile("model_(\\d+).backup")
	maxModel = 0

	modelsToRemove = []
	modelsToKeep = []

	for item in listdir(path):
		fullLocation = f"{path}/{item}"
		print(f"looking at {fullLocation} [{item}]")
		if isfile(fullLocation):
			print(f"is file")
			match = regex.match(item)
			print(f"match = {match}")
			#print(f"match len = {len(match.groups())}")
			#print(f"m = {(int)(match.group(1))}")
			if match is not None and len(match.groups()) >= 1:
				number = (int)(match.group(1))
				print(f"number = {number}")
				if (maxModel < number):
					maxModel = number
				if shouldRemoveModel(number):
					modelsToRemove.append((number, fullLocation, item))
				else:
					modelsToKeep.append((number, fullLocation, item))

	for fileEntry in modelsToRemove:
		(number, location, filename) = fileEntry
		if number == maxModel:
			print(f"Keeping because it's the highest model: {fileEntry}")
		else:
			print(f"Removing: {fileEntry}")
			Path(location).unlink()


	for fileEntry in modelsToKeep:
		(number, location, filename) = fileEntry
		print(f"Keeping: {fileEntry}")

