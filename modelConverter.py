#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#


import getopt
import sys
import torch

import openvino as ov
import numpy as np



from utilities import *
from trainer import *



def printHelp():
	print(f"Options:")
	print(f"            -h, --help                          show this help text")
	print(f"            -s, --size                          size of the input in the model")
	print(f"            -t, --torch=TORCH_MODEL             filename of the torch model to be converted to openvino model")
	print(f"            -o, --openvino=OPENVINO_MODEL       filename of the openvino model to convert the torch model to. Needs to end with .xml. \
	\n                                                Remember that it will also produce a .bin file that needs to be kept in the same directory.")





if __name__ == "__main__":

	torchFilename = ""
	openvinoFilename = ""

	
	n = 1024
	quantize = False
	

	try:
		arguments, values = getopt.getopt(sys.argv[1:], "ht:o:s:q", ["help", "torch=", "openvino=", "size=", "quantize"])
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-h", "--help"):
				printHelp()
				quit()

			elif currentArgument in ("-t", "--torch"):
				torchFilename = currentValue

			elif currentArgument in ("-o", "--openvino"):
				openvinoFilename = currentValue	

			elif currentArgument in ("-s", "--size"):
				n = int(currentValue)

			elif currentArgument in ("-q", "--quantize"):
				quantize = True

			

	
	except getopt.error as err:
		showErrorAndQuit(str(err))

	if (torchFilename == "" or openvinoFilename == ""):
		showErrorAndQuit("You need to provide both filenames.")

	if (not openvinoFilename.endswith(".xml")):
		showErrorAndQuit("The openvino filename needs to end with .xml. Remember that it will also produce a .bin file that needs to be kept in the same directory.")



	core = ov.Core()
	os.environ["MASTER_ADDR"] = "localhost"
	os.environ["MASTER_PORT"] = "12356"
	#torch.distributed.init_process_group(group_name="conversion", rank=0, world_size=1)
	
	#torchModel = torch.load(torchFilename, map_location=torch.device('cpu'))
	torchModel = utilities.getModelFromFileOrNew("typicalDeep", torchFilename, "cpu")

	print("torch:")
	print(torchModel)
	openvinoModel = ov.convert_model(torchModel, input=[1, 1, n, n])
	compiledOpenvinoModel = ov.compile_model(openvinoModel)
	print("openvino:")
	print(openvinoModel)
	if openvinoModel is None:
		print("OpenVino model is None!")


	if quantize:
		pass


	ov.save_model(openvinoModel, openvinoFilename)

	trainer = Trainer(torchModel, TaskInformation())
	exampleFile = "/home/tymon/Dokumenty/SmartLineartMine/Newest/ModelsComparison/input.png"
	input = utilities.prepareData(exampleFile)

	input_small = input[:, :, 0:n,0:n]

	outputTorch = torchModel(input_small)
	outputOpenvino = compiledOpenvinoModel(input_small)[0]
	print(f"shapes: {outputTorch.shape} {outputOpenvino.shape}")
	print(f"all numbers: {outputTorch.shape[0]*outputTorch.shape[1]*outputTorch.shape[2]*outputTorch.shape[3]}")

	print(f"sum input = {np.sum(input_small.detach().numpy())}")
	print(f"sum torch = {np.sum(outputTorch.detach().numpy())}")
	print(f"sum openvino: {np.sum(outputOpenvino)}")
	

	savedOpenvinoModel = ov.compile_model(openvinoFilename)
	if savedOpenvinoModel is None:
		print("savedOpenvinoModel is None!")
	
	outputSavedOpenvino = savedOpenvinoModel(input_small)[0]
	print(f"sum openvino saved: {np.sum(outputSavedOpenvino)}")

	
