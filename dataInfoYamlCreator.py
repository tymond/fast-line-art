#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#



from os import listdir
from os.path import isfile, join
import getopt
import sys
import yaml

from utilities import *


def printHelp():
	print(f"This script creates a yaml file out of content of three directories")
	print(f"Options:")
	print(f"            -h, --help                 show this help text")
	print(f"            -r, --raw=PATH             directory path of the data to process")
	print(f"            -v, --verification=PATH    directory path of the verification set")
	print(f"            -t, --training=PATH        directory path of the training set")
	print(f"            -o, --outputFile=PATH      filepath for the output yaml file")
	
	


def getFiles(directory: str):
	array = []
	for f in listdir(directory):
		if isfile(join(directory, f)):
			array.append(f)
	return array


def createDataInfoYaml(raw, training, verification):
	data = {'raw': getFiles(raw), 'verification': getFiles(verification + "/ink/"), 'training': getFiles(training + "/ink/")}
	return data

def saveYamlToFile(obj, filename):
	with open(filename, 'w') as stream:
		yaml.dump(obj, stream) 
	
	
	

if __name__ == "__main__":

	raw = ""
	verification = ""
	training = ""
	outputFile = ""

	try:
		arguments, values = getopt.getopt(sys.argv[1:], "hr:v:t:o:", ["help", "raw=", "verification=", "training=", "output="])
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-h", "--help"):
				printHelp()
				quit()
				
			elif currentArgument in ("-r", "--raw"):
				raw = currentValue
			
			elif currentArgument in ("-v", "--verification"):
				verification = currentValue

			elif currentArgument in ("-t", "--training"):
				training = currentValue
			
			elif currentArgument in ("-o", "--output"):
				outputFile = currentValue
				
	except getopt.error as err:
		showErrorAndQuit(str(err))

	if (raw == "" or training == "" or verification == ""):
		showErrorAndQuit(f"At least one of the paths is missing from the arguments list. (raw = {raw}, training = {training}, verification = {verification})")
	
	
	obj = createDataInfoYaml(raw, training, verification)
	saveYamlToFile(obj, outputFile)
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	
	
	
	
	
