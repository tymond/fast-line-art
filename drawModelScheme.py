#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#


from drawlib.apis import *
#import docs.style


import getopt
import sys
import time

import torchinfo as ts
import re

from trainer import *
from utilities import *


from os import listdir
from os.path import isfile, join, isdir

import inspect




def deb():
	config(width=100, height=50)



	circle(xy=(25, 30), radius=10)
	text(xy=(25, 10), text="Circle")
	rectangle(xy=(75, 30), width=20, height=20)
	text(xy=(75, 10), text="Rectangle")


	save()

def getModelData(model, width, height, device):
	inputSize = (1, width, height)

	#print(inspect.getsource(ts.summary))
	summary = ts.summary(model, inputSize,  device=device, verbose=0)

	#print(f"summary bef = [{summary}] {summary is None}")
	if summary is None:
		return
	summary = str(summary)
	#         Conv2d-1         [-1, 48, 256, 256]           1,248
	# ConvTranspose2d-7
	regex = re.compile(".*(Conv2d|ConvTranspose2d): (\\d+)\-(\\d+)\\s+(\\[.*\\])") #   \\w+(\[.*\])
	layerRegex = re.compile("\\[([\\d,]+), ([\\d,]+), ([\\d,]+)\\]")

	#print(f"summary = [{summary}] {summary is None}")
	layers = []
	lastOutputSide = 512
	lastOutputThickness = 1
	
	for line in summary.splitlines():
		if not line:
			next
		m = regex.match(line)
		if m:
			layer = m.group(4)
			print(f"layer = ({layer})")
			lm = layerRegex.match(layer)
			if lm:
				(outputThickness, side, side2) = (lm.group(1), lm.group(2), lm.group(3))
				print(f"and the numbers = {(outputThickness, side, side2)}")
				layers.append((int(lastOutputThickness), int(outputThickness), int(lastOutputSide), int(side)))
				lastOutputThickness = outputThickness
				lastOutputSide = side
	
	return layers

def drawModelIntoArea(layers, startX, startY, endX, endY, margin, thicknessScale, sideScale, useOutputThickness = True):

	
	height = endY - startY
	currentHeight = height - margin
	centerWidth = startX + (endX - startX)/2

	i = 0
	for layer in layers:
		(inputThickness, outputThickness, inputSide, outputSide) = layer
		if useOutputThickness:
			thickness = outputThickness
		else:
			thickness = inputThickness
		newHeight = currentHeight - thickness/thicknessScale

		polygon(xys=[(centerWidth-inputSide/(2*sideScale), currentHeight), (centerWidth+(inputSide/(2*sideScale)), currentHeight), 
			(centerWidth+outputSide/(2*sideScale), newHeight), (centerWidth-outputSide/(2*sideScale), newHeight)], text = f"{i}")
		print(f"Important numbers: inputSide = {inputSide}, outputSide = {outputSide}, used thickness: {thickness}")
		print(f"Made poligon with those points: {(centerWidth-inputSide/(2*sideScale), currentHeight)}, {(centerWidth+inputSide/(2*sideScale), currentHeight)}, \
			{(centerWidth+outputSide/(2*sideScale), newHeight)}, {(centerWidth-outputSide/(2*sideScale), newHeight)}")
		
		currentHeight = newHeight
		i += 1



def drawIntoImage(layers, filename):
	
	outputThicknessSum = 0
	inputThicknessSum = 0
	margin = 50
	sideMax = 0
	
	thicknessScale = 1
	sideScale = 2



	for layer in layers:
		(inputThickness, outputThickness, inputSide, outputSide) = layer
		outputThicknessSum += outputThickness
		inputThicknessSum += inputThickness
		sideMax = max(sideMax, outputSide)


	width = (2*sideMax/sideScale + 2*margin)
	height = (max(inputThicknessSum, outputThicknessSum)/thicknessScale + 2*margin)
	config(width = width, height = height)

	drawModelIntoArea(layers, 0, 0, width/2, height, margin, thicknessScale, sideScale, False)
	drawModelIntoArea(layers, width/2, 0, width, height, margin, thicknessScale, sideScale, True)
	
	
	save(file=filename)








if __name__ == "__main__":

	args = handleCommandlineOptions(sys.argv[1:], [("output", "o", 1, "path to the image file showing the model"), 
		("model", "m", 1, "path to the model"), ("modelType", "n", 1, "standarised codename for the model")])
	
	modelFile = ""
	outputFile = args["output"]
	modelType = ""


	if "model" in args:
		modelFile = args["model"]
	if "modelType" in args:
		modelType = args["modelType"]

	
	device = "cpu"
	model = None
	if (isfile(modelFile)):
		model = torch.load(modelFile, map_location=torch.device(device))
	else:
		model = modelTypeToNewModel(modelType)
	modelData = getModelData(model, 512, 512, device)
	drawIntoImage(modelData, outputFile)



