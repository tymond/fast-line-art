#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#

from os import listdir
from os.path import isfile, join, isdir
import getopt
import PIL.Image as Image
from PIL import ImageOps
import random
import shutil
import sys
from pathlib import Path
from random import sample, seed


from utilities import *
from taskfileParser import *


def printHelp():
	print(f"Options:")
	print(f"            -h, --help              show this help text")
	print(f"            -r, --raw=PATH          location of data to process, correctly named with ink_ and sketch_ (for example ink_dragon.png and sketch_dragon.png)")
	print(f"            -p, --processed=PATH    location of output processed data")
	print(f"            -d, --divided=PATH      location of output processed data divided into the training set and the verification set")





####################################################################################

# When writing the next version, KEEP IT DETERMINISTIC!!!
# Training set vs verification set is super important, and we might need to
#   create the images multiple times for the same training
# What about adding new images?
# One way would be to just have a list of result files with the info in which set they should land
#
# TODO: invert the pictures for better training

####################################################################################



def mirror(image):
	pass
	
def rotate(image):
	pass
	
	

# mirror
# rotate
# cut in four

def doOperations(sketch, lineart, depth, prefix, location):
	
	if lineart.entropy() < 1: # TODO: finetune the entropy - before, it was based on sketch, that's not a good idea
		return []
	
	(w, h) = sketch.size
	
	
	if depth > 0:
		im14 = sketch.crop((0, 0, w/2, h/2))
		im24 = sketch.crop((0, h/2, w/2, h))
		
		im34 = sketch.crop((w/2, 0, w, h/2))
		im44 = sketch.crop((w/2, h/2, w, h))
		
		li14 = lineart.crop((0, 0, w/2, h/2))
		li24 = lineart.crop((0, h/2, w/2, h))
		
		li34 = lineart.crop((w/2, 0, w, h/2))
		li44 = lineart.crop((w/2, h/2, w, h))
		
		
		doOperations(im14, li14, depth-1, prefix + "_14", location)
		doOperations(im24, li24, depth-1, prefix + "_24", location)
		doOperations(im34, li34, depth-1, prefix + "_34", location)
		doOperations(im44, li44, depth-1, prefix + "_44", location)
		
	for rot in [0, 90, 180, 270]:
		rott = sketch.rotate(rot)
		lirot = lineart.rotate(rot)
		
		
		
		filenameSketch = "sketch_" + prefix + "_{}_rot{}.png".format(depth, rot)
		filenameLineart = "ink_" + prefix + "_{}_rot{}.png".format(depth, rot)
		eprint("Saving picture {} with entropy {}".format(filenameSketch, sketch.entropy()))
		rott.save(location + filenameSketch)
		lirot.save(location + filenameLineart)


def createSizes(task, image):
	# TODO: use task info for sizes
	# now, it uses scale down 7% and scale up 7%
	#images = [image]
	up = 1.07
	down = 0.93
	sizeUp = (int(image.size[0]*up), int(image.size[1]*up))
	sizeUpImage = image.resize(sizeUp, Image.LANCZOS)
	
	sizeDown = (int(image.size[0]*down), int(image.size[1]*down))
	sizeDownImage = image.resize(sizeDown, Image.LANCZOS)

	return [image]
	return [sizeDownImage, image, sizeUpImage]
	


def createMirrors(task, images):
	response = []
	for image in images:
		response.append(image)
		response.append(ImageOps.mirror(image))
	return response


def createRotations(task, images):
	response = []
	for image in images:
		response.append(image)
		angle = 15
		for rot in range(angle, 360, angle):
			response.append(image.rotate(rot, resample= Image.BICUBIC, expand = True, fillcolor = "black"))
	
	return response

def createCuts(task, images):
	response = []
	for image in images:
		sizeCut = 1000
		cutMargin = int(0.1*1000)
		for i in range(0, image.width, sizeCut - cutMargin):
			for j in range(0, image.height, sizeCut - cutMargin):
				cropped = image.crop((i, j, min(i+sizeCut, image.width), min(j+sizeCut, image.height)))
				#print(f"Entropy: {cropped.entropy()}")
				#if cropped.entropy() < 10: # skip empty images
				#	print(f"Skipping...")
				#	next
				#else:
				#	print(f"Not skipping, because apparently ({cropped.entropy()} < 10) = {cropped.entropy() < 10}")
				response.append(cropped)
	
	return response



def invert(task, image):
	# TODO: use task info to get that
	print(f"{image.mode}")
	if (image.mode == "P"):
		print(f"{image.filename}")
	if (image.mode != "L"):
		image = image.convert("L")
	return ImageOps.invert(image)

def doAllOperationsNew(task, filenames, data):
	print(f"Do all operations:")
	response = []
	for i, image in enumerate(data):
		print(f"Operation 1. Invert")
		image = invert(task, image)
		print(f"Operation 2. Create sizes")
		images = createSizes(task, image)
		print(f"Operation 3. Create mirrors")
		images = createMirrors(task, images)
		print(f"Operation 4. Create rotations")
		images = createRotations(task, images)
		print(f"Operation 5. Create cuts")
		images = createCuts(task, images)
		imagesWithFilenames = []
		print(f"Ended up with: {len(images)}...")
		baseFilename = Path(filenames[i]).stem
		for j, im in enumerate(images):
			imagesWithFilenames.append((im, f"{baseFilename}_{j}"))
		response = response + imagesWithFilenames
		print(f"Ended up with: {len(images)}, total: {len(response)}")
	return response

def prepareData(task, sketches, sketchFiles, inks, inkFiles):
	sketchesOperationsResponse = doAllOperationsNew(task, sketchFiles, sketches)
	inksOperationsResponse = doAllOperationsNew(task, inkFiles, inks)
	assert(len(sketchesOperationsResponse) == len(inksOperationsResponse))

	sketchesList = []
	inksList = []

	count = 0
	for i in range(len(sketchesOperationsResponse)):
		minEntropy = 0.1
		if sketchesOperationsResponse[i][0].entropy() < minEntropy or inksOperationsResponse[i][0].entropy() < minEntropy:
			# skip
			print(f"Skipping because of entropy")
			pass
		else:
			sketchesList.append(sketchesOperationsResponse[i])
			inksList.append(inksOperationsResponse[i])
			print(f"Appending to files")
			count += 1
	
	sketchesOperationsResponse = sketchesList
	inksOperationsResponse = inksList
	print(f"Lists counts = {count} {len(sketchesOperationsResponse)} {len(inksOperationsResponse)}")


	random.seed(1234)
	percentageInVerification = 10
	imagesCount = len(sketchesOperationsResponse)
	verificationSetCount = int(percentageInVerification*0.01*imagesCount)
	indices = random.sample(list(range(imagesCount)), verificationSetCount)
	print(f"Now dividing into two sets, train: {imagesCount - verificationSetCount} verify: {verificationSetCount}")

	k = 0
	for i in indices:
		# those are in verification
		(sketch, sketchFilenameBase) = sketchesOperationsResponse[i]
		(ink, inkFilenameBase) = inksOperationsResponse[i]
		
		sketchFilenameBase = sketchFilenameBase.replace("sketch", "image")
		inkFilenameBase = inkFilenameBase.replace("ink", "image")
		


		sketch.save(f"{task.pathsOptions.validationSetPath}/sketch/{sketchFilenameBase}.png")
		ink.save(f"{task.pathsOptions.validationSetPath}/ink/{inkFilenameBase}.png")

		print(f"Saved {k} images (verification): sk:{sketchFilenameBase} i:{inkFilenameBase} ({sketch.entropy()}, {ink.entropy()})")
		k += 1
	

	for i in range(imagesCount):
		if i in indices:
			next
		# those are in training
		(sketch, sketchFilenameBase) = sketchesOperationsResponse[i]
		(ink, inkFilenameBase) = inksOperationsResponse[i]

		sketchFilenameBase = sketchFilenameBase.replace("sketch", "image")
		inkFilenameBase = inkFilenameBase.replace("ink", "image")

		sketch.save(f"{task.pathsOptions.trainingSetPath}/sketch/{sketchFilenameBase}.png")
		ink.save(f"{task.pathsOptions.trainingSetPath}/ink/{inkFilenameBase}.png")
		print(f"Saved {k} images (verify + train: sk:{sketchFilenameBase} i:{inkFilenameBase}) ({sketch.entropy()}, {ink.entropy()}) ")
		k += 1
	
	
		





def readData(task, sketchFiles = [str], inkFiles = [str]):
	sketches = []
	inks = []
	base = task.pathsOptions.getRawDataFullPath()
	for i, sketchFile in enumerate(sketchFiles):
		sketchData = None
		inkData = None
		inkFile = inkFiles[i]
		try:
			sketchData = Image.open(f"{base}/{sketchFile}")
			inkData = Image.open(f"{base}/{inkFile}")
		except Exception as e:
			eprint(f"Cannot load either {sketchFile} or {inkFile} as image. {e}")
		finally:
			sketches.append(sketchData)
			inks.append(inkData)
	
	return (sketches, inks)


def divideIntoSketchAndInk(files = [str]):
	files = sorted(files)
	print(f"Sorted list of files: {files}")
	sketches = []
	inks = []

	iit = 0
	sketchit = 0
	for i in range(len(files)):
		name = str(files[i])
		if (name.startswith("sketch_")):
			sketchit = i
			break
	if sketchit == 0:
		showErrorAndQuit(f"Wrong list of raw files. {files}")
	print(f"Sketch it = {sketchit}")
	while(iit < len(files) and sketchit < len(files)):
		ink = str(files[iit])
		sketch = str(files[sketchit])
		print(f"ink = {ink}, sk = {sketch}")

		if not ink.startswith("ink_"):
			iit += 1
			next
		if not sketch.startswith("sketch_"):
			sketchit += 1
			next

		baseInk = ink.removeprefix("ink_")
		baseSketch = sketch.removeprefix("sketch_")

		if (baseInk != baseSketch):
			if (baseInk < baseSketch):
				iit += 1
				next
			else:
				sketchit += 1
				next
		sketches.append(sketch)
		inks.append(ink)
		sketchit += 1
		iit += 1
		
	return (sketches, inks)

	



if __name__ == "__main__":

	raw = "data_raw/"
	processed = "data_processed/"
	divided = "data_divided/"

	taskfile = ""
	rawFilenamesList = ""
	clean = False

	try:
		arguments, values = getopt.getopt(sys.argv[1:], "ht:c", ["help", "taskfile=", "rawFilenamesList=", "clean"])
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-h", "--help"):
				printHelp()
				quit()
				
			elif currentArgument in ("-t", "--taskfile"):
				taskfile = currentValue

			elif currentArgument in ("-r", "--rawFilenamesList"):
				rawFilenamesList = currentValue

			elif currentArgument in ("-c", "--clean"):
				clean = True

			

			
				
	except getopt.error as err:
		showErrorAndQuit(str(err))

	if (clean):
		# TODO
		pass

	if (not isfile(taskfile)):
		showErrorAndQuit("There is no file called {taskfile}")

	if (rawFilenamesList != "" and not isfile(rawFilenamesList)):
		showErrorAndQuit("There is no file called {rawFilenamesList}")


	task = TaskFileParser().parseFile(taskfile)

	# all operations in order
	# also, first get the actual raw data files!
	# if rawFilenames != "", use that
	# otherwise, use all from data/raw
	
	rawFilenames = []
	if rawFilenamesList != "":
		with open(rawFilenamesList, "r") as listFile:
			rawFilenames = listFile.readlines()
	else:
		print(f"Trying to get data from here: {task.pathsOptions.getRawDataFullPath()}. {listdir(task.pathsOptions.getRawDataFullPath())}")
		for item in listdir(task.pathsOptions.getRawDataFullPath()):
			print(f"Item is: {item} is it a file? {isfile(item)}")
			if isfile(f"{task.pathsOptions.getRawDataFullPath()}/{item}"):
				rawFilenames.append(item)
	
	# ok, now operations
	print(f"Stage 1. Dividing into sketch and ink files.")
	(sketchFiles, inkFiles) = divideIntoSketchAndInk(rawFilenames)
	print(f"Sketch files: {sketchFiles}")
	print(f"Ink files: {inkFiles}")
	
	print(f"Stage 2. Reading data from sketch and ink files.")
	(sketchData, inkData) = readData(task, sketchFiles, inkFiles)
	print(f"Stage 3. Augmenting data and dividing into training and verification set.")
	prepareData(task, sketchData, sketchFiles, inkData, inkFiles)
	print(f"Preparing data finished.")









	quit()
	#######








	divided_training = divided + "/training/"
	divided_verify = divided + "/verify/"

	divided_training_ink = divided_training + "/ink/"
	divided_training_sketch = divided_training + "/sketch/"

	divided_verify_ink = divided_verify + "/ink/"
	divided_verify_sketch = divided_verify + "/sketch/"


	# sanity checks
	if not isdir(raw):
		showErrorAndQuit("The location specified as raw data location doesn't exist.")
		
	ensureExistence(processed, "processed data location")
	ensureExistence(divided, "divided data location")
	ensureExistence(divided_training, "divided/training data location")
	ensureExistence(divided_verify, "divided/verify data location")
	ensureExistence(divided_training_ink, "divided/training/ink data location")
	ensureExistence(divided_verify_ink, "divided/verify/ink data location")
	ensureExistence(divided_training_sketch, "divided/training data location")
	ensureExistence(divided_verify_sketch, "divided/verify/sketch data location")		
	
	# do operations and save into "processed" folder
	for f in listdir(raw):
		if isfile(join(raw, f)):
			if "sketch" in f:
				sketch = f
				ink = f.replace("sketch", "ink")
				base = f.replace("sketch_", "")
				base = base.replace(".png", "")
				with Image.open(raw + sketch) as sketchImage:
					with Image.open(raw + ink) as inkImage:
						sketchImage = sketchImage.convert("L")
						doOperations(sketchImage, inkImage, 1, base, processed)
						
	sketches = []

	percForTraining = 0.9
	random.seed(10)

	# divide into two folders
	for f in listdir(processed):
		if isfile(join(processed, f)):
			if "sketch" in f:
				# only sketch files
				loc = divided
				if random.random() < percForTraining:
					loc = divided + "/training/"
				else:
					loc = divided + "/verify/"
			
				newname = f.replace("sketch_", "")
				shutil.copy(join(processed, f), loc + "/sketch/" + newname)
				liname = f.replace("sketch_", "ink_")
				shutil.copy(join(processed, liname), loc + "/ink/" + newname)
			
			







