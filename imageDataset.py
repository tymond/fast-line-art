#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#




from torch.utils.data import Dataset


import utilities


class ImageDatasetOnDemand(Dataset):
	folderPath : str = None
	sketchFilepaths : [] = None
	inkFilepaths : [] = None
	

	def __init__(self, folderPath: str):
		self.folderPath = folderPath
		self._load()

	def _load(self):
		(sketches, inks) = utilities.getImageFilepaths(self.folderPath)
		self.sketchFilepaths = sketches
		self.inkFilepaths = inks

	def __len__(self):
		return len(self.sketchFilepaths)
	
	def __getitem__(self, index):
		#print(f"Trying to get item {index}, we have: {self.sketchFilepaths[index]} and {self.inkFilepaths[index]}, returning {(self.sketchFilepaths[index], self.inkFilepaths[index])}")
		return (self.sketchFilepaths[index], self.inkFilepaths[index])



class ImageDatasetAtOnce(Dataset):

	filepathsLoader : ImageDatasetOnDemand = None
	sketchesData : [] = None
	inksData : [] = None

	def __init__(self, folderPath: str):
		self.folderPath = folderPath
		self._load()
		assert False, "To be done in the future"

	def _load(self):
		self.filepathsLoader = ImageDatasetOnDemand(self.folderPath)
		self.sketchesData = []
		self.inksData = []
		
		for sketch in self.filepathsLoader.sketchFilepaths:
			data = utilities.prepareData(sketch)
			self.sketchesData.append(data)





