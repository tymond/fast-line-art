#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#

import torch
import getopt
import sys
from os import listdir
from os.path import isfile, join, isdir

from torchvision import transforms
from PIL import Image
from torchvision.utils import save_image



from modelsDefinitions import *
from taskfileParser import *


def printHelp():
	print(f"Options:")
	print(f"            -h, --help              show this help text")
	print(f"            -p, --path    			path to all the experiments")
	




if __name__ == "__main__":
	
	path = ""
	try:
		arguments, values = getopt.getopt(sys.argv[1:], "hp:", ["help", "path="])
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-h", "--help"):
				printHelp()
				quit()
			elif currentArgument in ("-p", "--path"):
				path = currentValue
	except getopt.error as err:
		showErrorAndQuit(str(err))

	if (path == ""):
		showErrorAndQuit("Please provide a path to the folder with experiments folders")

	parser = TaskFileParser()
	for folder in listdir(path):
		newPath = os.path.join(path, folder)
		if folder == "repo":
			continue
		#print(f"folder = {folder}")
		if isdir(newPath):
			for file in listdir(newPath):
				if file == "taskfile.yml":
					print(f"~~~~~~~~~~ {folder} ~~~~~~~~~~")
					ti : TaskInformation = parser.parseFile(os.path.join(newPath, file))
					print(f"note: {ti.note}")
					
					print(f"model type: {ti.trainingOptions.modelType}")
					print(f"loss function: {ti.trainingOptions.lossFunction}")
					print(f"optimizer: {ti.trainingOptions.optimizer}")
					print(f"learning rate: {ti.trainingOptions.learningRate}")
					
					print(f"\n")

	



