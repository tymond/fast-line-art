#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import torch
from torch.utils.data import Dataset, DataLoader
from torch.nn.parallel import DistributedDataParallel as DDP

import datetime
import time

from devicesProcessesInfo import DevicesProcessesInfo
from taskfileParser import *
import utilities
import metaHandler
import signal
import sys


class Trainer:
	def __init__(
		self,
		model: torch.nn.Module,
		task: TaskInformation,
		train_data: DataLoader,
		val_data: DataLoader,
		optimizer: torch.optim.Optimizer,
		worker_id: int,
		device: str,
		save_every: int,
		devicesProcessesInfo: DevicesProcessesInfo
	) -> None:
		self.device = device
		self.worker_id = worker_id
		#print(f"Device to use here {self.worker_id}: {device}")
		self.model = model.to(device)
		self.train_data = train_data
		self.val_data = val_data
		self.optimizer = optimizer
		self.save_every = save_every
		self.model = DDP(model) # here usually you'd use device_ids # also this seems to synchronise models???
		self.devicesProcessesInfo = devicesProcessesInfo
		self.task = task


	def _save_checkpoint(self, epoch, train_loss, val_loss):
		assert self.worker_id == 0, "saving checkpoint on a wrong worker"

		metaHandler.updateTrainingMeta(self.task, epoch, train_loss, val_loss, self.model.module)

		return
		#####
		ckp = self.model.module.state_dict()
		PATH = "checkpoint.pt"
		torch.save(ckp, PATH)
		print(f"Epoch {epoch} | Training checkpoint saved at {PATH} | model = {self.model.module.state_dict()}")
		n = 10
		s = ""
		for i in range(n):
			res = self.model(torch.tensor([i/float(n)]))
			s += f"{res[0]}, "
		print(f"{s}")
		# here validate the epoch as well

	def _run_batch(self, source, targets):
		self.optimizer.zero_grad()

		times = 1 if self.device == "cuda" else 1
		for i in range(times):
			#print(f"Types: {source.dtype}")
			#print(f"Data here ({self.worker_id}): {source}")
			output = self.model(source)
			#print(f"Output for {self.worker_id}: {output}")
			loss = torch.nn.MSELoss()(output, targets)
			#loss2 = loss * 4 + loss

		#output = self.model(source)
		#print(f"Output for {self.worker_id}: {output}")
		#loss = torch.nn.MSELoss()(output, targets)
		#loss2 = loss * 4 + loss

		#print(f"Loss for {self.worker_id}: {loss}")
		
		loss.backward()
		self.optimizer.step()
		#print(f"Result for {self.worker_id}: {self.model.module.state_dict()}")

	def _run_multibatch(self, source, targets, validate=False):
		#print(f"what did I get? {source} {source.shape}")
		self.optimizer.zero_grad()
		cumulative_loss = 0
		local_volume = source.shape[0]

		for i in range(local_volume):
			local_source = source[i]

			#print(f"Trying with input shape: {local_source.shape} from {source[i].shape}")
			output = self.model(source[i])
			loss = torch.nn.MSELoss()(output, targets[i])
			cumulative_loss += loss
		
		cumulative_loss = cumulative_loss/local_volume
		if not validate:
			cumulative_loss.backward()
			self.optimizer.step()
		
		return cumulative_loss

	def _run_multibatch_list(self, source, targets, validate=False):
		#print(f"what did I get? {source} {source.shape}")
		#print(f"[worker = {self.worker_id}] _run_multibatch_list start (1)", file=sys.stderr)
		self.optimizer.zero_grad()
		cumulative_loss = torch.tensor(0.0, device=self.device)
		
		local_volume = len(source)
		#print(f"[worker = {self.worker_id}] _run_multibatch_list start (2)", file=sys.stderr)

		for i in range(local_volume):
			#print(f"[worker = {self.worker_id}] _run_multibatch_list start (3, start it)", file=sys.stderr)
			local_source = source[i]

			#print(f"Trying with input shape: {local_source.shape} from {source[i].shape}")
			#print(f"[worker = {self.worker_id}] _run_multibatch_list start (4)", file=sys.stderr)
			output = self.model(source[i])
			#print(f"[worker = {self.worker_id}] _run_multibatch_list start (5)", file=sys.stderr)
			loss = torch.nn.MSELoss()(output, targets[i])
			#print(f"[worker = {self.worker_id}] _run_multibatch_list start (6)", file=sys.stderr)
			cumulative_loss += loss
			#print(f"[worker = {self.worker_id}] _run_multibatch_list start (7)", file=sys.stderr)
		
		#print(f"[worker = {self.worker_id}] _run_multibatch_list start (8)", file=sys.stderr)
		cumulative_loss = cumulative_loss/local_volume
		#print(f"[worker = {self.worker_id}] _run_multibatch_list start (9)", file=sys.stderr)
		if not validate:
			#print(f"[worker = {self.worker_id}] _run_multibatch_list start (10): c_loss: {cumulative_loss}, reqiuires grad: {cumulative_loss.requires_grad}, local volume was: {local_volume}", file=sys.stderr)
			cumulative_loss.backward()
			#print(f"[worker = {self.worker_id}] _run_multibatch_list start (11)", file=sys.stderr)
			self.optimizer.step()
			#print(f"[worker = {self.worker_id}] _run_multibatch_list start (12)", file=sys.stderr)
		
		#print(f"[worker = {self.worker_id}] _run_multibatch_list start (13)", file=sys.stderr)
		return cumulative_loss


	def _run_epoch(self, epoch, validate=False):

		before = time.time()

		dataLoader = self.val_data if validate else self.train_data

		b_sz = len(next(iter(dataLoader))[0])
		dataLoader.sampler.set_epoch(epoch)
		volume = self.devicesProcessesInfo.volumePerRank(self.worker_id)
		#print(f"[worker = {self.worker_id}] Epoch {epoch} | Batchsize: {b_sz} | len(dataloader): {len(dataLoader)} | volume: {volume}", file=sys.stderr)
		
		v = 0
		#cumulative_source = None
		#cumulative_targets = None

		cumulative_source_list = None
		cumulative_targets_list = None
		

		cumulative_loss_all = torch.tensor(0.0, device=self.device)

		batches_count = 0

		it = 0

		for (source, targets) in dataLoader:

			print(f"[worker = {self.worker_id}] Iteration {it} | v = {v} | len(cumullist) = {-1 if (cumulative_source_list is None) else len(cumulative_source_list)}", file=sys.stderr)
			it += 1

			#print(f"Size of the input: {source.shape}")
			#print(f"Input: {source}, {targets}")
			(source) = source
			(targets) = targets
			source = source[0]
			targets = targets[0]
			
			#print(f"Input [{self.worker_id}]: {source}, {targets}")
			
			
			if type(source) == str:
				source = utilities.prepareData(source)
				targets = utilities.prepareData(targets)
				

			source = source.to(self.device)
			targets = targets.to(self.device)
			source = source.type(torch.float32)
			targets = targets.type(torch.float32)
			
			if cumulative_source_list is None:

				#cumulative_source = source.unsqueeze(0)
				#cumulative_targets = targets.unsqueeze(0)
				cumulative_source_list = [source]
				cumulative_targets_list = [targets]
				
				#print(f"shape = {cumulative_source.shape}")
				v = 1
			else:
				#print(f"shapes of the tensors: cumulative source: {cumulative_source.shape}, source.unsqueeze: {source.unsqueeze(0).shape}", file=sys.stderr)
				#cumulative_source = torch.cat((cumulative_source, source.unsqueeze(0)), dim=0)
				#cumulative_targets = torch.cat((cumulative_targets, source.unsqueeze(0)), dim=0)
				cumulative_source_list = cumulative_source_list.append(source)
				cumulative_targets_list = cumulative_targets_list.append(targets)
				
				v += 1
			
			#print(f"[worker = {self.worker_id}] Iteration {it} | v = {v} | len(cumullist) (1, after adding) = {-1 if (cumulative_source_list is None) else len(cumulative_source_list)}", file=sys.stderr)

			if v == volume:
				#print(f"[worker = {self.worker_id}] Iteration {it} | v = {v} | Finally starting a multibatch_list", file=sys.stderr)
				#print(f"Running a multibatch starting with shape: {cumulative_source.shape} and data: {cumulative_source}")
				loss = self._run_multibatch_list(cumulative_source_list, cumulative_targets_list, validate) # includes a STEP
				#print(f"[worker = {self.worker_id}] Iteration {it} | v = {v} | during if: len(cumulative_source_list) = {-1 if (cumulative_source_list is None) else len(cumulative_source_list)}, loss = {loss}", file=sys.stderr)
				cumulative_loss_all += loss * len(cumulative_source_list)
				batches_count += len(cumulative_source_list)
				cumulative_source_list = None
				cumulative_targets_list = None
			
			#print(f"[worker = {self.worker_id}] Iteration {it} | v = {v} | len(cumullist) (2, after if) = {-1 if (cumulative_source_list is None) else len(cumulative_source_list)}", file=sys.stderr)
			

		#print(f"[worker = {self.worker_id}] Iteration {it} AFTER EVERYTHING | v = {v} | len(cumullist) = {-1 if (cumulative_source_list is None) else len(cumulative_source_list)}", file=sys.stderr)	

		if cumulative_source_list is not None:
			loss = self._run_multibatch_list(cumulative_source_list, cumulative_targets_list, validate)
			#print(f"[worker = {self.worker_id}] Iteration {it}, after every | v = {v} | len(cumulative_source_list) = {len(cumulative_source_list)}, loss = {loss}", file=sys.stderr)
			cumulative_loss_all += loss * len(cumulative_source_list)
			batches_count += len(cumulative_source_list)
		
		cumulative_loss_tensor = torch.tensor([cumulative_loss_all.item(), batches_count])
		cumulative_loss_tensor.to("cpu")
		#print(f"Cumulative loss tensor in ({self.worker_id}) before sync: {cumulative_loss_tensor}, made from: {cumulative_loss_all}, {batches_count}", file=sys.stderr)


		after = time.time()
		print(f"[worker = {self.worker_id}] Training everything took {datetime.timedelta(seconds=(after - before))}", file=sys.stderr)

		torch.distributed.all_reduce(cumulative_loss_tensor, op=torch.distributed.ReduceOp.SUM)
		cumulative_loss_tensor.to(self.device)
		#print(f"Cumulative loss tensor in ({self.worker_id} after sync: {cumulative_loss_tensor})")

		cumulative_loss_true = cumulative_loss_tensor[0]/cumulative_loss_tensor[1]
		return cumulative_loss_true

		

	def train(self, startEpoch: int, total_epochs: int):

		def sigintHandler(signum, _frame):
			
			signal.signal(signum, signal.SIG_IGN) # ignore future signals
			print(f"We got signal to finish work: {signum}", file=sys.stderr)
			if self.worker_id == 0:
				#endTrainingMeta(task, currentEpoch)
				endTrainingMetaGuessEpoch(task)
			else:
				print(f"Cannot save the work, worker id = {self.worker_id}", file=sys.stderr)
			sys.exit(0)
		
		signal.signal(signal.SIGINT, sigintHandler)
		signal.signal(signal.SIGHUP, sigintHandler)
		signal.signal(signal.SIGTERM, sigintHandler)

		currentEpoch = startEpoch
		for epoch in range(startEpoch, startEpoch + total_epochs):
			print(f"[{self.worker_id}] Starting epoch {epoch} at: " + datetime.datetime.now().strftime("%H:%M:%S"))
			self.model.train(True)
			train_loss = self._run_epoch(epoch)
			self.model.train(False)
			val_loss = self._run_epoch(epoch, True)
			#if self.worker_id == 0 and epoch % self.save_every == 0:
			#	self._save_checkpoint(epoch)
			#print(f"[{self.worker_id}] End epoch {epoch} at: " + datetime.datetime.now().strftime("%H:%M:%S"))
			
			
			if self.worker_id == 0 and epoch % self.save_every == 0:
				self._save_checkpoint(epoch, train_loss, val_loss)
				print(f"[{self.worker_id}] End epoch {epoch} with tl: {train_loss} vl: {val_loss}")
			
			print(f"[{self.worker_id}] End epoch {epoch}")
			self.model.train(True)
			#print(f"Current module looks like this ({self.worker_id}): {self.model.module.state_dict()}")
