#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


from torch.utils.data.sampler import Sampler

from torch.utils.data.distributed import DistributedSampler


class WeightedDistributedSampler(Sampler):

	samplers = None


	def __init__(self, dataset, start_data_rank, count_ranks, num_replicas: int) -> None:
		self.samplers = []
		for data_rank in range(start_data_rank, start_data_rank + count_ranks):
			self.samplers.append(DistributedSampler(dataset, rank = data_rank, num_replicas=num_replicas))
		
	def __iter__(self):
		for sampler in self.samplers:
			for data in sampler:
				yield data

	def __len__(self) -> int:
		l = 0
		for sampler in self.samplers:
			l += len(sampler)
		return l

	def set_epoch(self, epoch: int) -> None:
		for sampler in self.samplers:
			sampler.set_epoch(epoch)

