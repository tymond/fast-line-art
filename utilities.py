#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#

import sys
import torch

from os.path import isdir, isfile, join
from os import mkdir, listdir

import getopt
from modelsDefinitions import *
from PIL import Image, ImageOps
from torchvision.utils import save_image

from torchvision import transforms


from modelsDefinitions import *

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def showErrorAndQuit(errorStr: str):
	print(f"ERROR: {errorStr}")
	quit()


def getDevice(default = "cuda", userPreferred = "cuda"):
	if userPreferred == "":
		userPreferred = default
	if userPreferred == "cuda" and torch.cuda.is_available():
		return "cuda"
	else:
		return "cpu"

def ensureExistence(location: str, locationName: str):
	if not isdir(location):
		mkdir(location)
		if not isdir(location):
			showErrorAndQuit(f"The location specified as {locationName} doesn't exist and cannot be created.")


def getOptimizer(optimizer, model, learningRate):
	if optimizer == "adadelta":
		return torch.optim.Adadelta(model.parameters())
	else:
		return torch.optim.SGD(model.parameters(), learningRate)

def getLossFunction(lossFunction):
	if lossFunction == "blackWhite":
		return blackWhiteLoss
	else:
		return torch.nn.MSELoss()


def guessModelFormat(modelPath: str):
	if modelPath.endswith(".ml") or ((modelPath.endswith(".backup") and not modelPath.endswith("pt.backup"))):
		return "ml"
	else:
		return "pt"


def getModelFromFileOrNew(modelType: str, modelPath: str, device: str, modelFormat: str = None):
	# TODO: if current model exists, use that
	# if not, create the model using the type
	model = None
	if modelFormat is None:
		modelFormat = guessModelFormat(modelPath)

	if (isfile(modelPath)):
		if modelFormat == "ml":
			model = torch.load(modelPath, map_location=torch.device(device))
			if model is None:
				showErrorAndQuit(f"Cannot load model from file {modelPath}")
		else:
			model = modelTypeToNewModel(modelType)
			#model = SpecialCustomModel(SpecialCustomModel(SpecialCustomModel(SpecialCustomModel(SpecialCustomModel(model)))))
			stateDict = torch.load(modelPath, weights_only=True, map_location=torch.device(device))
			if model is None:
				showErrorAndQuit(f"Cannot load model from file {modelPath}")
			(missing, unexpected) = model.load_state_dict(stateDict)
			if len(missing) > 0 or len(unexpected) > 0:
				showErrorAndQuit(f"Cannot load model from file {modelPath} (mixup with model type): missing: {missing}, unexpected: {unexpected}")
	else:
		model = modelTypeToNewModel(modelType)
		if model is None:
			showErrorAndQuit(f"Cannot create a model of type: {modelType}")
	
	print(f"Model file = {modelPath}, type: {modelType}, format: {modelFormat}")
	model.to(device)
	model.train(True)
	
	return model



def saveModel(currentModel: nn.Module, path: str):
	#modelFormat: str = "ml" if (path.endswith("ml")) else "pt"
	both = True
	if both:
		torch.save(currentModel, path + "_ml.ml")
		ckp = currentModel.state_dict()
		torch.save(ckp, path + "_pt.pt")
	#else:
	modelFormat: str = "pt" if (path.endswith(".pt") or path.endswith("pt.backup")) else "ml"
	if modelFormat == "ml":
		torch.save(currentModel, path)
	else:
		ckp = currentModel.state_dict()
		torch.save(ckp, path)
		


def handleCommandlineOptions(providedOptions: [str], descriptions: [str]):
	# options = [(optionname, optionletter, takes argument?, description for Usage)]

	optionLetters = "h"
	optionNames = ["help"]
	for option in descriptions:
		(name, letter, takesArg, descr) = option
		optionLetters += letter
		if takesArg:
			optionLetters += ":"
			name += "="
		optionNames.append(name)
	
	helpText = f"Usage:\n{descriptions}"

	response = {}

	try:
		arguments, values = getopt.getopt(providedOptions, optionLetters, optionNames)
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-h", "--help"):
				print(helpText)
				quit()

			for option in descriptions:
				(name, letter, takesArg, descr) = option
				if currentArgument in (f"-{letter}", f"--{name}"):
					response[name] = currentValue

	except getopt.error as err:
		showErrorAndQuit(str(err))


		
	return response


def prepareData(filename):
	
	image = Image.open(filename)
	(w, h) = image.size
	#print(f"image size before = {image.size}")
	image = image.crop((0, 0, 8*(int(w/8)), 8*(int(h/8)))) # ensures %8 == 0
	image = image.convert("L")
	#print(f"image size = {image.size}")
	data = transforms.ToTensor()(image)
	data = torch.unsqueeze(data, 0)
	return data

def prepareDataWithExactSize(filename, size):
	image = Image.open(filename)
	image = image.crop((0, 0, size, size))
	image = image.convert("L")
	#print(f"image size = {image.size}")
	data = transforms.ToTensor()(image)
	data = torch.unsqueeze(data, 0)
	return data


def prepareDataWithCutting(filename, maxTrainingImageSize):

	# all current models require the image to be divisable by 8
	magicNumber = 8
	
	image = Image.open(filename)
	(w, h) = image.size
	maxSize = maxTrainingImageSize
	maxSize = maxSize - maxSize%magicNumber # decrease to divisable, so it doesn't go over intended maxSize
	# it does introduce a problem of empty context for pixels on the edges
	# but it shouldn't be that big of a problem, as long as the maxSize isn't too tiny

	if (w%magicNumber != 0 or h%magicNumber != 0):
		wdiff = w%magicNumber
		hdiff = h%magicNumber
		w = w + wdiff # increase to divisable, so it includes the whole pictures (doesn't shave off edges)
		h = h + hdiff
		image = ImageOps.expand(image, (wdiff, hdiff), "black")
		#print(f"Full image size changed to: ({w}, {h})")
	
	#print(f"Image size = {image.width()} {image.height()}")
	image = image.convert("L")
	

	dataList = []

	if w > maxSize or h > maxSize:
		imageArea = w*h
		for i in range(0, w, maxSize):
			for j in range(0, h, maxSize):
				part = image.crop((i, j, min(w, i + maxSize), min(h, j + maxSize)))
				#print(f"Part image size = ({part.width()}, {part.height()}")
				weight = part.width*part.height/imageArea
				data = transforms.ToTensor()(part)
				data = torch.unsqueeze(data, 0)
				#print(f"Types of data added to list (1): {type(data)}")
				dataList.append([data, weight])

	else:
		data = transforms.ToTensor()(image)
		data = torch.unsqueeze(data, 0)
		#print(f"Types of data added to list (2): {type(data)}")
		dataList.append([data, 1])

	#print(f"Types of data added to list (3): {type(dataList[0][0])}")
	return dataList


	
def saveAsImage(self, data, filename):
	data = data.squeeze(0)
	save_image(data, filename)

def getImageFilepaths(location: str):
	sketches = []
	inks = []
	
	sketchesLoc = location + "/sketch/"
	inksLoc = location + "/ink/"
	
	
	for f in listdir(sketchesLoc):
		sketchFile = join(sketchesLoc, f)
		inkFile = join(inksLoc, f)
		if isfile(sketchFile) and isfile(inkFile):
			sketches.append(sketchFile)
			inks.append(inkFile)
	return (sketches, inks)