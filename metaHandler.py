#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#

import datetime as dt
import csv
import shutil

from os.path import isfile
from enum import Enum

import utilities
from utilities import *
from taskfileParser import *

class StatisticsField(Enum):
	Epoch = "Epoch"
	EpochLoss = "EpochLoss"
	EpochVerifyLoss = "EpochVerifyLoss"
	EpochModelFile = "EpochModelFile"



def shouldContinueTraining(task: TaskInformation):
	if isfile(task.pathsOptions.getMainModelFileFullPath()) \
		and isfile(task.pathsOptions.getFullPath(task.pathsOptions.statisticsFile)) \
		and isfile(task.pathsOptions.getFullPath(task.pathsOptions.metaFile)) \
		and isfile(task.pathsOptions.getFullPath(task.pathsOptions.metaArchiveFile)):
		return True
	
	return False


def startTraining(task, dry=False, ignoreCrash=False):
	if shouldContinueTraining(task):
		return continueTraining(task, dry, ignoreCrash)
	else:
		return startNewTraining(task, dry)
		

def checkStartTraining(task, ignoreCrash=False):
	if shouldContinueTraining(task):
		return checkTrainingContinuation(task, ignoreCrash)
	else:
		return True

# sooo, what does it do:

# training start:
# - create epoch statistics file
# - create meta file with info
# - create meta archive file with info


def startNewTraining(task, dry=False):
	ps = task.pathsOptions
	
	if isfile(ps.getMainModelFileFullPath()):
		showErrorAndQuit("If you want to start a new training, remove the main model file first.")
	
	if not dry:
		
		statisticsFile = ps.getFullPath(ps.statisticsFile)
		with open(statisticsFile, "w") as stats:
			stats.write(f"Epoch;Training Loss;Validation Loss;Backup Model File\n")
		
		time = dt.datetime.now(dt.timezone.utc)
		
		with open(ps.getFullPath(ps.metaFile), "w") as meta:
			meta.write(f"0;{time.isoformat(' ')}\n")
		
		with open(ps.getFullPath(ps.metaArchiveFile), "a") as metaArchive:
			# TODO: give more info
			metaArchive.write(f"------------------------\n")
			metaArchive.write(f"0;{time.isoformat(' ')}\n")
			
	return 0



# training restart:
# - get the epoch number from the meta file
# - replace the start training info in the meta file
# - add new start in the meta archive file

def checkTrainingContinuation(task, ignoreCrash=False):
	ps : PathsOptions = task.pathsOptions
	### IMPORTANT: find the last model backup and start from there, if the model.ml file is missing! <- needed for crashes, loss of power etc.
	lines = []
	
	with open(ps.getFullPath(ps.metaFile), "r") as meta:
		lines = meta.readlines()
	
	if len(lines) >= 2:
		parts = lines[1].split(";")
		epoch = int(parts[0])
	else:
		print(f"[QUITTING (diff thread)] PLEASE FIX EPOCHS in meta.txt FIRST", file=sys.stderr)
		sys.exit(1)

def continueTraining(task, dry=False, ignoreCrash=False):
	ps : PathsOptions = task.pathsOptions
	
	time = dt.datetime.now(dt.timezone.utc)
	
	# TODO: figure out the epoch number from meta
	

	### IMPORTANT: find the last model backup and start from there, if the model.ml file is missing! <- needed for crashes, loss of power etc.
	lines = []
	
	
	with open(ps.getFullPath(ps.metaFile), "r") as meta:
		lines = meta.readlines()
	
	if len(lines) >= 2:
		parts = lines[1].split(";")
		epoch = int(parts[0])
	else:
		print(f"[QUITTING] PLEASE FIX EPOCHS in meta.txt FIRST", file=sys.stderr)
		sys.exit(1)

	
		

	if not dry:
			
		with open(ps.getFullPath(ps.metaFile), "w") as meta:	
			meta.write(f"{epoch};{time.isoformat(' ')}\n")
		
		with open(ps.getFullPath(ps.metaArchiveFile), "a") as metaArchive:
			# TODO: give more info
			metaArchive.write(f"------------------------\n")
			metaArchive.write(f"{epoch};{time.isoformat(' ')}\n")
			
	return epoch


# during training:
#   update epoch number
#   update epoch loss
#   update epoch verify loss
#   update model backup name (if relevant)

def updateTrainingMeta(task : TaskInformation, epoch : int, loss : float, verifyLoss : float, currentModel: nn.Module):
	print(f"Update Training Meta", file=sys.stderr)
	ps = task.pathsOptions

	backupModelFile = ""

	#mainModelPath = ps.getMainModelFileFullPath()
	utilities.saveModel(currentModel, ps.getMainModelFileFullPath())



	if ((epoch + 1)%task.trainingOptions.savingModelBackupFrequency == 0):
		
		backupModelFile = task.pathsOptions.getModelBackupsFileFullPathForEpoch(epoch)
		print(f"Backup model file = {backupModelFile}")
	else:
		print(f"Won't save a backup because {(epoch + 1)} % {task.trainingOptions.savingModelBackupFrequency} = {(epoch + 1)%task.trainingOptions.savingModelBackupFrequency}")


	
	with open(ps.getFullPath(ps.statisticsFile), "a") as stats:
		stats.write(f"{epoch};{loss};{verifyLoss};{backupModelFile};{dt.datetime.now(dt.timezone.utc)}\n")
	
	if backupModelFile != "":
		if isfile(backupModelFile):
			print(f"The backup file: {backupModelFile} already exists!", file=sys.stderr)
			endTrainingMetaGuessEpoch(task)
			sys.exit(1)
		utilities.saveModel(currentModel, backupModelFile)
	


# training stop:
#   save epoch number and stop time in the meta archive file
#   save epoch number in the meta file


def endTrainingMeta(task, epoch):

	ps = task.pathsOptions
	
	time = dt.datetime.now(dt.timezone.utc)
	
	with open(ps.getFullPath(ps.metaFile), "a") as meta:
		meta.write(f"{epoch};{time.isoformat(' ')}\n")
	
	with open(ps.getFullPath(ps.metaArchiveFile), "a") as metaArchive:
		# TODO: give more info
		metaArchive.write(f"{epoch};{time.isoformat(' ')}\n")

def endTrainingMetaGuessEpoch(task: TaskInformation):
	ps = task.pathsOptions

	with open(ps.getFullPath(ps.statisticsFile), newline='') as stats:
		r = csv.reader(stats, delimiter=";")
		next(r, None)  # skip the headers
		# columns are: {epoch}; {loss}; {verifyLoss}; {backupModelFile}; {dt.datetime.now(dt.timezone.utc)}
		lastEpoch = -1
		for row in r:
			if len(row) >= 1:
				try:
					lastEpoch = int(row[0])
				except Exception:
					pass

		
	endTrainingMeta(task, lastEpoch + 1)


