#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow
from PyQt5.QtChart import QChart, QLineSeries, QChartView, QCategoryAxis, QValueAxis
from PyQt5.QtWidgets import QDialog, QHBoxLayout, QVBoxLayout, QPushButton, QToolButton, QCheckBox, QFileDialog, QLabel, QTextEdit, QLineEdit, QRadioButton, QMessageBox, QGridLayout, QDoubleSpinBox
from PyQt5.QtGui import QImage
from PyQt5.QtCore import QBuffer, QByteArray, pyqtSignal, Qt, QPointF



import sys
import csv
from pathlib import *


class ExperimentGuiOptions:
	showRaw : bool = True
	showSmooth : bool = True

class ExperimentData:



	filename = ""
	rawTrainingSetData = []
	rawVerificationSetData = []

	smoothedOutBy5 = []
	smoothedOutBy5Training = []

	guiOptions : ExperimentGuiOptions = ExperimentGuiOptions()




	def __init__(self):
		guiOptions = ExperimentGuiOptions()
		pass

	def getDataPoints(self):
		series = QLineSeries()
		series.append(5.0, 10.0)
		series.append(3.0, 12.0)
		return series
	
	def loadDataFromFile(self, filename):
		self.filename = filename
		self.rawTrainingSetData = []
		self.rawVerificationSetData = []
		
		with open(filename, newline='') as csvfile:
			datareader = csv.reader(csvfile, delimiter=';')
			for i, row in enumerate(datareader):
				if i == 0:
					continue
				#print(f"Row: [{row}]")
				self.rawTrainingSetData.append(float(row[1]))
				self.rawVerificationSetData.append(float(row[2]))

		self.smoothenOut()

	def convertToPoints(self, dataSet):
		response = []
		for i, data in enumerate(dataSet):
			response.append(QPointF(i, data))
		return response

	def smoothenOut(self):
		self.smoothedOutBy5 = self.smoothenOutData(self.rawVerificationSetData)
		self.smoothedOutBy5Training = self.smoothenOutData(self.rawTrainingSetData)
		

	def smoothenOutData(self, dataArray):
		margin = 50
		results = []

		_sum = 0
		for i, data in enumerate(dataArray):
			if i < margin or i > (len(dataArray) - margin - 2):
				sumLocal = 0
				rangeMin = max(0, i - margin)
				rangeMax = min(len(dataArray), i + margin + 1)
				for j in range(rangeMin, rangeMax):
					sumLocal += dataArray[j]
				results.append(sumLocal/(rangeMax - rangeMin))

			else:
				if i == margin:
					for j in range(0, 2*margin + 1):
						_sum += dataArray[j]
						#print(f"To the initial sum, adding d[{j}] (sum = {_sum})")
				else:
					_sum -= dataArray[i-margin -1]
					_sum += dataArray[i+margin]
					#print(f"We're at {i}, Taking out d[{i - margin -1 }] and adding d[{i + margin}]")
				#print(f"Sum = {_sum} and the amount to divide by is {(2*margin + 1)} and the result: {_sum/(2*margin + 1)}")
				results.append(_sum/(2*margin + 1))
		return results




class MainWindow(QMainWindow):

	windowWidget = None
	chart = None
	chartViewWidget = None
	
	loadingWidget = None
	optionsWidget = None

	experimentsData = []
	


	def __init__(self):
		super().__init__()

		self.setWindowTitle("Compare Experiments")
		
		self.experimentsData = [ExperimentData(), ExperimentData()]

		self.createChartWidget()
		self.createLoadingWidget()
		self.createOptionsWidget()

		self.windowWidget = QWidget()
		
		mainLayout = QVBoxLayout()
		

		mainLayout.addWidget(self.loadingWidget)
		mainLayout.addWidget(self.optionsWidget)
		mainLayout.addWidget(self.chartViewWidget)


		self.windowWidget.setLayout(mainLayout)

		self.setCentralWidget(self.windowWidget)

		self.resize(1000, 800)

	def createChartWidget(self):
		self.chart = QChart()
		#self.chart.addSeries(ExperimentData().getDataPoints())
		
		#axisX = QCategoryAxis()
		#axisY = QCategoryAxis()
		
		#axisX.append("low", 10)
		#axisX.append("optimal", 20)
		#axisX.append("high", 30)
		#axisX.setRange(0, 30)
		#axisY.append("slow", 10)
		#axisY.append("med", 20)
		#axisY.append("fast", 30)
		#axisY.setRange(0, 30)
		#self.chart.addAxis(axisX, Qt.AlignBottom)
		#self.chart.addAxis(axisY, Qt.AlignLeft)


		self.chartViewWidget = QChartView(self.chart)

	def createLoadingWidget(self):
		self.exp1TextBox = QLineEdit()
		self.exp2TextBox = QLineEdit()
		
		exp1FileButton = QPushButton("")
		exp2FileButton = QPushButton("")

		exp1FileButton.clicked.connect(self.loadExperimentFirst)
		exp2FileButton.clicked.connect(self.loadExperimentSecond)
		
		
		layout = QGridLayout()
		layout.addWidget(self.exp1TextBox, 0, 0)
		layout.addWidget(self.exp2TextBox, 1, 0)
		layout.addWidget(exp1FileButton, 0, 1)
		layout.addWidget(exp2FileButton, 1, 1)

		self.loadingWidget = QWidget()
		self.loadingWidget.setLayout(layout)
	
	def createOptionsWidget(self):
		labelAxisX = QLabel("Horizontal/X Axis Range:")
		labelAxisY = QLabel("Vertical/Y Axis Range:")

		self.minAxisXSpin = QDoubleSpinBox()
		self.maxAxisXSpin = QDoubleSpinBox()
		
		self.minAxisYSpin = QDoubleSpinBox()
		self.maxAxisYSpin = QDoubleSpinBox()

		spins = [self.minAxisXSpin, self.maxAxisXSpin, self.minAxisYSpin, self.maxAxisYSpin]
		for spin in spins:
			spin.setDecimals(10)
			spin.valueChanged.connect(self.axesLimitsChanged)
			spin.setRange(0, 1000000)

		axisXLayout = QHBoxLayout()
		axisYLayout = QHBoxLayout()

		axisXLayout.addWidget(self.minAxisXSpin)
		axisXLayout.addWidget(self.maxAxisXSpin)

		axisYLayout.addWidget(self.minAxisYSpin)
		axisYLayout.addWidget(self.maxAxisYSpin)
		

		layout = QVBoxLayout()
		layout.addWidget(labelAxisX)
		layout.addLayout(axisXLayout)
		layout.addWidget(labelAxisY)
		layout.addLayout(axisYLayout)

		self.optionsWidget = QWidget()
		self.optionsWidget.setLayout(layout)

		

	def loadExperimentFirst(self):
		self.loadExperiment(0)

	def loadExperimentSecond(self):
		self.loadExperiment(1)

	

	def loadExperiment(self, no):
		print(f"Loading experiment {no}.")
		filename = QFileDialog.getOpenFileName(self, f"Load experiment {no}")[0]
		if no == 0:
			self.exp1TextBox.setText(filename)
		else:
			self.exp2TextBox.setText(filename)

		print(f"Setting experiment as {no}.")

		print(f"Experiments before: {len(self.experimentsData[0].rawVerificationSetData)}, {len(self.experimentsData[1].rawVerificationSetData)}, \
			ids: {id(self.experimentsData[0])}, {id(self.experimentsData[1])}, ids of lists: {id(self.experimentsData[0].rawVerificationSetData)}, {id(self.experimentsData[1].rawVerificationSetData)}")
		self.experimentsData[no] = ExperimentData()
		self.experimentsData[no].loadDataFromFile(filename)
		print(f"Experiments: {len(self.experimentsData[0].rawVerificationSetData)}, {len(self.experimentsData[1].rawVerificationSetData)}")
		
		self.refreshChart()
	
	def refreshChart(self):
		self.chart.removeAllSeries()
		for axis in self.chart.axes():
			self.chart.removeAxis(axis)
		axisXRange = QPointF(0.0, 0.0)
		axisYRange = QPointF(0.0, 0.0)


		def expName(filename):
			path = PurePath(filename)
			absolute = path.absolute
			parts = absolute.parts
			return parts[-2] + "/" +parts[-1]
			
		
		for exp in self.experimentsData:
			if len(exp.rawVerificationSetData) > 0 and exp.guiOptions.showRaw:
				print(f"Adding series: {exp.filename} as {len(self.chart.series())}. serie because it's {len(exp.rawVerificationSetData)}")
				series = QLineSeries()
				axisXRange.setY(max(axisYRange.y(), len(exp.rawVerificationSetData)))
				#axisYRange.setx(mIN(axisYRange.y(), len(exp.rawVerificationSetData)))
				#minY = min(exp.rawVerificationSetData)
				minY = 0
				maxY = max(exp.rawVerificationSetData)
				axisYRange = QPointF(min(minY, axisYRange.y()), max(maxY, axisXRange.x()))
				
				series.append(exp.convertToPoints(exp.rawVerificationSetData))
				series.setName(exp.filename)
				self.chart.addSeries(series)
		
		for exp in self.experimentsData:
			if len(exp.smoothedOutBy5) > 0  and exp.guiOptions.showSmooth:
				series = QLineSeries()
				series.append(exp.convertToPoints(exp.smoothedOutBy5))
				series.setName(f"smooth_{exp.filename}")
				self.chart.addSeries(series)
			
			if len(exp.smoothedOutBy5Training) > 0  and exp.guiOptions.showSmooth:
				series = QLineSeries()
				series.append(exp.convertToPoints(exp.smoothedOutBy5Training))
				series.setName(f"smooth_train_{exp.filename}")
				self.chart.addSeries(series)
			
			



		axisX = QValueAxis()
		axisY = QValueAxis()


		self.minAxisXSpin.setValue(axisXRange.x())
		self.maxAxisXSpin.setValue(axisXRange.y())
		self.minAxisYSpin.setValue(axisYRange.x())
		self.maxAxisYSpin.setValue(axisYRange.y())

		axisX.setRange(axisXRange.x(), axisXRange.y())
		axisY.setRange(axisYRange.x(), axisYRange.y())
		
		for serie in self.chart.series():
			self.chart.setAxisX(axisX, serie)
			self.chart.setAxisY(axisY, serie)
		
		
		#self.chart.createDefaultAxes()
		#axisY = self.chart.axisY()
		#axisY.setMin(0)

		#print(f"Set values: {self.chart.axisX().min()} {self.chart.axisX().max()} {self.chart.axisY().min()} {self.chart.axisY().max()}")

		#for serie in self.chart.series():
		#	serie.attachAxis(self.chart.axisX())
		#	serie.attachAxis(self.chart.axisY())
		

		


	def axesLimitsChanged(self):
		
		axisX = self.chart.axisX()
		if axisX is None:
			return
		axisX.setRange(self.minAxisXSpin.value(), self.maxAxisXSpin.value())
		
		axisY = self.chart.axisY()
		axisY.setRange(self.minAxisYSpin.value(), self.maxAxisYSpin.value())

		#for serie in self.chart.series():
		#	serie.attachAxis(self.chart.axisX())
		#	serie.attachAxis(self.chart.axisY())

		#self.chart.setAxisX(axisX)
		#axisX = QValueAxis()
		#axisX.setRange(self.minAxisXSpin.value(), self.maxAxisXSpin.value())
		
		#self.chart.setAxisX(axisX)
		
		



	
	



if __name__ == "__main__":

	app = QApplication(sys.argv)
	window = MainWindow()
	window.show()
	app.exec()