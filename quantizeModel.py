#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#



import openvino as ov
import numpy as np
import torch
from sklearn.metrics import mean_squared_error
import nncf

import argparse

from imageDataset import *

import utilities






if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Quantize model')
	parser.add_argument('-d', '--device', type=str, help="Device to run it on")
	parser.add_argument('-i', '--input', type=str, help="Path to folder with input images")
	parser.add_argument('-m', '--model', type=str, help="Path to openvino model")
	parser.add_argument('-o', '--outputModel', type=str, help="Path to the output openvino model")
	parser.add_argument('-r', '--maxDrop', type=str, help="Max accuracy drop")
	parser.add_argument('-s', '--subsetSize', type=str, help="Subset size for the number of the input data folder samples")
	
	
	#parser.add_argument('-s', '--model', type=str, help="Path to openvino model")
	
	# TODO: add torch models as well
	

	args = parser.parse_args()

	assert(args.input is not None)
	assert(args.model is not None)
	assert(args.outputModel is not None)
	

	inputFolder = args.input
	modelPath = args.model

	dataset = ImageDatasetOnDemand(inputFolder)
	calibrationLoader = torch.utils.data.DataLoader(dataset, batch_size = 1, shuffle=True)
	dataset2 = ImageDatasetOnDemand(inputFolder)
	validationLoader = torch.utils.data.DataLoader(dataset, batch_size = 1, shuffle=True)
	


	model = ov.Core().read_model(model=modelPath)


	
	size = model.input().get_shape()[2]



	def transformFn(filenameList):
		images = []
		return utilities.prepareDataWithExactSize(filenameList[0][0], size)
	
	def transformFnForValidation(filenameList):
		images = []
		return utilities.prepareDataWithExactSize(filenameList[1][0], size)
	
	


	calibrationDataset = nncf.Dataset(calibrationLoader, transformFn)
	validationDataset = nncf.Dataset(validationLoader, transformFnForValidation)

	# code from openvino manual
	def validate(model: ov.CompiledModel, 
				validationLoader: torch.utils.data.DataLoader) -> float:
		predictions = []
		references = []

		output = model.outputs[0]

		for imagePath, targetPath in validationLoader:
			#print(f"images in validation loader: {images} {target}")
			image = utilities.prepareDataWithExactSize(imagePath[0], size)
			target = utilities.prepareDataWithExactSize(targetPath[0], size)
			pred = model(image)[output]

			pred = pred.squeeze(0)
			target = target.squeeze(0)


			pred = pred.squeeze(0)
			target = target.squeeze(0)
			
			
			predictions.append(pred)
			references.append(target)

		predictions = np.concatenate(predictions, axis=0)
		references = np.concatenate(references, axis=0)
		#print(f"shape of the predictions: {predictions.shape}")
		return mean_squared_error(predictions, references)

	subsetSize = int(args.subsetSize) if (args.subsetSize is not None) else 300
	maxDrop = float(args.maxDrop) if (args.maxDrop is not None) else 0.01
	

	quantized_model = nncf.quantize_with_accuracy_control(
		model,
		calibration_dataset=calibrationDataset,
		validation_dataset=validationDataset,
		validation_fn=validate,
		max_drop=maxDrop,
		drop_type=nncf.DropType.ABSOLUTE,
		subset_size=subsetSize,
	)


	ov.save_model(quantized_model, args.outputModel, compress_to_fp16=False)
