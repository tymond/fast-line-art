#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#

import getopt
import sys
import time

from torchsummary import summary


from trainer import *
from utilities import *


def printHelp():
	print(f"Options:")
	print(f"            -h, --help              show this help text")
	print(f"            -m, --modelType=TYPE    type of model to show the summary of")
	print(f"            -w, --width=WIDTH       width of the input image")
	print(f"            -t, --height=HEIGHT     height of the input image")
	print(f"            -d, --device=DEVICE     preferred device to run the summary on")
	

	

def showSummary(modelType: str, width: int, height: int, preferredDevice: str):
	if preferredDevice != "":
		device = getDevice(preferredDevice)
	else: 
		device = getDevice()

	model = modelTypeToNewModel(modelType)
	if model == None:
		showErrorAndQuit("Unknown model type.")
		
	model.to(device)

	#model.seq[0].weight.shape
	kernelSizesMinusOneSum = 0
	for layer in model.seq:
		if hasattr(layer, "weight"):
			print(layer.weight.shape)
			kernelSizesMinusOneSum += (int)(layer.weight.shape[2]/2)

	
	inputSize = (1, width, height)
	#inputSize.to(device)
	#exampleInput = torch.rand(inputSize, device=device)
	summary(model, inputSize, device = device)

	print(model.seq.type)
	print(f"----\nContext window: {kernelSizesMinusOneSum} pixels in all directions (so, in theory, {kernelSizesMinusOneSum*2 + 1}x{kernelSizesMinusOneSum*2 + 1})")




if __name__ == "__main__":

	model = None
	
	height = 100
	width = 100
	modelType = "tooSmallConv"
	preferredDevice = ""

	try:
		arguments, values = getopt.getopt(sys.argv[1:], "ht:w:m:d:", ["help", "height=", "width=", "modelType=", "device="])
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-h", "--help"):
				printHelp()
				quit()
			
			elif currentArgument in ("-t", "--height"):
				height = int(currentValue)
				
			elif currentArgument in ("-w", "--width"):
				width = int(currentValue)
				
			elif currentArgument in ("-m", "--modelType"):
				modelType = currentValue
			
			elif currentArgument in ("-d", "--device"):
				preferredDevice = currentValue
				
	except getopt.error as err:
		showErrorAndQuit(str(err))

	before = time.time()
	showSummary(modelType, width, height, preferredDevice)
	after = time.time()
	spent = after - before
	iterations : int = int(60*60*1000/spent)
	iterationsK : int = int(iterations/1000)
	iterationsStr = str(iterationsK) + "k" if iterationsK > 0 else str(iterations)

	print(f"Getting the summary took {spent/1000}s, meaning it will have {iterationsStr} iterations in 1h if it was only summary and not training...")
	

