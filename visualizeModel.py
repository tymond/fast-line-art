#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

import matplotlib.pyplot as plt
import numpy as np
import torch



from trainer import *

def plotModelFilter(model, layerId, title):
    layer = model.seq[layerId]
    weights : np.array = layer.weight.data


    plt.figure()
    plt.axis("off")
    #plt.imshow(np.transpose(weights, (1, 2, 0)))
    print(weights[1,:,:,:].shape)
    plt.imshow(np.squeeze(weights[1,:,:,:]))
    plt.title(title)
    plt.show()



model = torch.load("/home/tymon/Dokumenty/SmartLineartMine/Newest/TooSmallConvBlackWhite/models/model.ml", "cpu")
#plotModelFilter(model, 0, "lalala")

model.eval()
activations = {}
def get_activation(name):
    def hook(model, input, output):
        activations[name] = output.detach()
    return hook

model.seq[4].register_forward_hook(get_activation('result'))

trainer = Trainer(model, TaskInformation())

with torch.no_grad():
    output = model(trainer.prepareData("./data_divided/training/sketch/i_2_rot0.png"))


# Visualization function for activations
def plot_activations(layer, num_cols=4, num_activations=16):
    num_kernels = layer.shape[1]
    fig, axes = plt.subplots(nrows=(num_activations + num_cols - 1) // num_cols, ncols=num_cols, figsize=(12, 12))
    for i, ax in enumerate(axes.flat):
        if i < num_kernels:
            ax.imshow(layer[0, i].cpu().numpy(), cmap='twilight')
            ax.axis('off')
    plt.tight_layout()
    plt.show()

# Display a subset of activations
plot_activations(activations['result'], num_cols=4, num_activations=16)

