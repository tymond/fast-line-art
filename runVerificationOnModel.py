#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#



from os import listdir
from os.path import isfile, join
import re
import time
from datetime import timedelta

from modelsDefinitions import *
from utilities import *
from trainer import *


def calculateForModel(modelPath: str, dataPath: str, device: str, lossFn):
	invert = False
	
	loss = 0
	imagesList = listdir(f"{dataPath}/sketch/")
	if len(imagesList) == 0:
		return infinity


	for image in imagesList:

		model = torch.load(modelPath, map_location=torch.device(device), weights_only=False)
		model.train(False)
		
		#print(f"model is = {model}, from {modelPath}")
		infinity = 1000.0
		if model is None:
			return infinity
	

		tr = Trainer(model, TaskInformation())




		before = time.time()
		sketchPath = f"{dataPath}/sketch/{image}"
		inkPath = f"{dataPath}/ink/{image}"
		
		inputData = tr.prepareData(sketchPath)



		if invert:
			inputData = 1 - inputData
		inputData = inputData.to(device)

		outputData = model(inputData)

		
		#continue


		outputData = outputData.to("cpu")
		if invert:
			outputData = 1 - outputData
		expectedOutput = tr.prepareData(inkPath)
		loss += float(lossFn(outputData, expectedOutput))

		after = time.time()
		delta = timedelta(seconds = after - before)
		print(f"Loss for {modelPath}, image {image}: {loss}, took: {delta}")
		del model
		del tr
	return loss/len(imagesList)

		





if __name__ == "__main__":


	args = handleCommandlineOptions(sys.argv[1:], [("data", "i", 1, "path to the folder with verification data/images (must contain both sketch and ink folders)"), 
		("modelsFolder", "m", 1, "path to the folder with models"), ("outputFile", "o", 1, "path to the output file (csv)"), ("lossFunction", "l", 1, "loss function codename (like mse, blackWhite)"),
		("device", "d", 1, "preferred device codename (cpu or cuda)"), ("shortVersion", "s", 0, "use if you want just the most important models")])

	

	modelsFolder = args["modelsFolder"]
	outputFile = args["outputFile"]
	lossFunctionName = args["lossFunction"]
	dataFolder = args["data"]
	preferredDevice = args["device"]
	
	shortVersion = False

	if "shortVersion" in args:
		shortVersion = True


	preferredDevice = getDevice("cuda", preferredDevice)
	lossFn = blackWhiteLoss if lossFunctionName == "blackWhite" else meanSquareLoss
	
	losses = {}

	for model in listdir(modelsFolder):
		modelPath = f"{modelsFolder}/{model}"
		if isfile(modelPath):
			regex = re.compile("model_(\\d+).backup")
			match = regex.match(model)
			#print(f"match = {match}")
			if match is not None and len(match.groups()) >= 1:
				number = (int)(match.group(1))
				#print(f"number = {number}")

				if shortVersion:
					shouldCheckThisModel = (number == 0 or number == 100 or number == 500 or number%1000 == 0)
					if not shouldCheckThisModel:
						continue
				
				before = time.time()
				loss = calculateForModel(modelPath, dataFolder, preferredDevice, lossFn)
				#loss = 1000
				after = time.time()
				delta = timedelta(seconds = after - before)
				print(f"### Loss for model {model} ({number}): {loss}, took: {delta}")
				losses[number] = (number, modelPath, loss)


#for key in keys(losses):
#	pass 




	
	


	