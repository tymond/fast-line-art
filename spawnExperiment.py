#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#


import time
import numpy as np
import matplotlib.pyplot as plt
import datetime
import getopt
import utilities as utils
import sys
import signal

from os import listdir
from os.path import isfile, join
from sklearn.utils import shuffle



from trainer import *
from taskfileParser import *
from metaHandler import *





def printHelp():
    print(f"Options:")
    print(f"            -h, --help                          show this help text")
    print(f"            -p, --path=PATH                     location of the folder for the experiment")
    print(f"            -n, --note=NOTE                     human-readable note about the experiment (optional, but recommended)")


def createTaskfile(location: str, note):
    # location doesn't contain the filename
    content = """
# Task file

# what is used now:
# optimizer
# learning rate, if optimizer == sgd

# human-readable note about the training
note: """
    content += note
    content +="""Shallow, inverted data, both pictures

# DATA and DATA AUGMENTATION PROCESS
# everything about making and converting the data
data:
    dataset raw version: 0
    dataset augmented divided version: 0
    
    invert: 1
    rotate90: 0
    rotate10: 1
    scaleup10: 1 # scale up 10 %
    scaledown10: 1 # scale down 10 %
    mirror: 1
        
    
    option x: 0
    option y: 0

# MODEL


# TRAINING
training:
    batch size: # might be related to dataset
    optimizer: adadelta # options - adadelta, sgd
    learning rate: 0.001 # 
    loss function: mse # options - mse, blackWhite
    brush file: 
    brush type: 
    model type: shallowModel # options - tooSmallConv, 
    saving model backup frequency: 1
    

# PATHS
paths:
"""
    content += "    main directory: " + location + "/" + " # all the paths below are relative to the base path"
    content += """    
    statistics file: ./statistics.csv
    meta file: ./meta.txt
    meta archive file: ./metaArchive.txt

        
    model backups pattern: ./models/model_NNNN.backup
    dataset info pattern: ./data/info/dataset-NNNN.txt

        
    main model file: ./models/model.ml
    raw data path: ./data/raw/
    training set path: ./data/training/
    validation set path: ./data/verify/
    data info yaml dir: ./data/info/ # a specific yaml file will be selected based on the dataset version
    
    

# OTHER
debug:
    logging: False
"""

    with open(location + "/taskfile.yml", "w") as file:
        file.write(content)
    




def createExperiment(path: str, note: str):
    # create all folders
    # then, create the taskfile
    dataPath = path + "/data/"
    modelsPath = path + "/models/"
    
    mkdir(modelsPath)
    mkdir(dataPath)

    mkdir(modelsPath + "/backups/")
    mkdir(dataPath + "/info/")
    mkdir(dataPath + "/raw/")

    mkdir(dataPath + "/training/")
    mkdir(dataPath + "/training/ink/")
    mkdir(dataPath + "/training/sketch/")
    

    mkdir(dataPath + "/verify/")
    mkdir(dataPath + "/verify/ink/")
    mkdir(dataPath + "/verify/sketch/")
    
    createTaskfile(path, note)



if __name__ == "__main__":

    note = ""
    path = ""

    try:
        arguments, values = getopt.getopt(sys.argv[1:], "hn:p:", ["help", "note=", "path="])
        for currentArgument, currentValue in arguments:
            if currentArgument in ("-h", "--help"):
                printHelp()
                quit()

            elif currentArgument in ("-n", "--note"):
                note = currentValue

            elif currentArgument in ("-p", "--path"):
                path = currentValue

    
    except getopt.error as err:
        showErrorAndQuit(str(err))


    if (path == ""):
        showErrorAndQuit("Provide a path to the folder for the experiment")

    # TODO: ensure the path to that path exists
    if (not isdir(path)):
        mkdir(path)

    if (not isdir(path)):
        showErrorAndQuit(f"{path} cannot be created, make sure to provide a correct pathh.")

    createExperiment(path, note)














