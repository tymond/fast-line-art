#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#

import torch


from torchvision import transforms
from PIL import Image, ImageOps
from torchvision.utils import save_image


from modelsDefinitions import *
from taskfileParser import *
import utilities


class Trainer:
	
	optimizer = None
	lossFn = None
	
	epochLosses = []
	verifyLosses = []
	
	
	taskInformation : TaskInformation = None
	
	
	
	
	
	def __init__(self, model: torch.nn.Module, taskInformation: TaskInformation):
		
		# TODO: loss function should be our own with a special loss map
		self.taskInformation = taskInformation
		self.optimizer = utilities.getOptimizer(self.taskInformation.trainingOptions.optimizer, model, self.taskInformation.trainingOptions.learningRate)
		self.lossFn = utilities.getLossFunction(self.taskInformation.trainingOptions.lossFunction)
		
		#print(self.lossFn)
		
		
		
		
		
		
		
	
	
	def trainOneImage(self, model: torch.nn.Module, device: str, sketch, expected, epoch_index = 0):
		#print(f"Types of data variables: {type(sketch)} {type(expected)}")
		sketchData, inkData = sketch.to(device), expected.to(device)
		
		# zeroing gradients for the next step
		self.optimizer.zero_grad()
		
		output = model(sketchData)
		loss = self.lossFn(output, inkData)
		
		loss.backward()
		
		# Adjust learning weights
		self.optimizer.step()
		return loss.item()
		
		
		
	def trainOneEpoch(self, model: torch.nn.Module, device, finput: list[str], fexpected: list[str], epoch_index = 0):
		
		allLoss = 0
		assert(len(finput) == len(fexpected))
		
		for i, sketch in enumerate(finput):
			#print(f"{sketch} {fexpected[i]}")
			print(f"Image {i}.: ", end='')
			sketchDataList = utilities.prepareDataWithCutting(sketch, self.taskInformation.trainingOptions.maxTrainingImageSize)
			inkDataList = utilities.prepareDataWithCutting(fexpected[i], self.taskInformation.trainingOptions.maxTrainingImageSize)
			avgImageLoss = 0
			for j, sketchDataInfo in enumerate(sketchDataList):
				sketchData = sketchDataInfo[0]
				inkData = inkDataList[j][0]
				#print(f"shapes of data: {sketchData.shape} {inkData.shape}")
				#print(f"types of data variables: {type(sketchData)} {type(inkData)}")
				lastLoss = self.trainOneImage(model, device, sketchData, inkData, epoch_index)
				avgImageLoss += lastLoss * sketchDataInfo[1]

			print(f"loss {avgImageLoss}")
			allLoss += avgImageLoss
		
		epochLoss = allLoss/len(finput)
		self.epochLosses.append(epochLoss)
		return epochLoss
		
		
	def verifyOneImage(self, model: torch.nn.Module, device: str, sketch, expected, epoch_index = 0):
		sketchData, inkData = sketch.to(device), expected.to(device)
		
		output = model(sketchData)
		loss = self.lossFn(output, inkData)
		return loss.item()
		
		
		
	def verifyOneEpoch(self, model: torch.nn.Module, device, finput: list[str], fexpected: list[str], epoch_index = 0):
		
		lastLoss = 0
		allLoss = 0
		assert(len(finput) == len(fexpected))
		
		for i, sketch in enumerate(finput):
			
			sketchData = utilities.prepareData(sketch)
			inkData = utilities.prepareData(fexpected[i])
			lastLoss = self.verifyOneImage(model, device, sketchData, inkData, epoch_index)
			allLoss += lastLoss
		
		verifyLoss = allLoss/len(finput)
		self.verifyLosses.append(verifyLoss)
		return verifyLoss
		
		
