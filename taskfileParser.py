#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#


import yaml
import os

from utilities import showErrorAndQuit


class DataAugmentationOptions:
	
	datasetVersion = 0
	datasetAugmentedDividedVersion = 0


	rawDataPath = ""
	rawDataFilenamesDir = ""
	
	dividedDataPath = ""
	dividedDataFilenamesDir = ""
	
	

	
	def mapVersionToFilenamesPath(self, raw: bool):
		
		prefix = "divided_"
		if raw:
			prefix = "raw_"
		
		return prefix + "dataset_" + str(self.datasetVersion) + ".txt"
	
	
	def checkIntegrity(self):
		# TODO: implement
		return True
	
	
class TrainingOptions:
	
	modelType = ""
	learningRate = 0.001
	optimizer = "adadelta"
	savingModelBackupFrequency = 1
	lossFunction = "mse"
	maxTrainingImageSize = 4*1024 # note: depends on the device!!!
	
	
	def checkIntegrity(self):
		if self.modelType == "":
			return False
		if self.optimizer != "sgd" and self.optimizer != 'adadelta':
			return False
		if self.optimizer == 'sgd' and self.learningRate == 0:
			return False
		if self.lossFunction not in ["mse", "blackWhite"]:
			return False
		return True
		

	
class PathsOptions:
	mainDirectory = ""
	modelBackupsPattern = ""
	
	mainModelFile = ""
	rawDataPath = ""
	trainingSetPath = ""
	validationSetPath = ""
	
	statisticsFile = ""
	metaFile = ""
	metaArchiveFile = ""
	
	
	
	def checkIntegrity(self):
		# TODO: implement
		return True
	
	def getFullPath(self, path):
		return self.mainDirectory + "/" + path
	
	def getRawDataFullPath(self):
		return self.mainDirectory + "/" + self.rawDataPath

	def getTrainingSetFullPath(self):
		return self.mainDirectory + "/" + self.trainingSetPath
		
	def getValidationSetFullPath(self):
		return self.mainDirectory + "/" + self.validationSetPath
		
	def getMainModelFileFullPath(self):
		return self.mainDirectory + "/" + self.mainModelFile
		
	def getModelBackupsFileFullPathForEpoch(self, epoch):
		filepath = self.mainDirectory + "/" + self.modelBackupsPattern.replace("NNNN", str(epoch).zfill(5))
		print(f"Model backups file path = {filepath}")
		return filepath
		
		
	
class TaskInformation:
	
	trainingOptions = TrainingOptions()
	dataAugmentationOptions = DataAugmentationOptions()
	pathsOptions = PathsOptions()
	note = ""
	
	
	
	def checkIntegrity(self):
		# TODO: implement correctly, checking the paths, whether the model files exist etc., 
		# preferably in the corresponding classes
		if (not self.trainingOptions.checkIntegrity()):
			return False
		
		if (not self.dataAugmentationOptions.checkIntegrity()):
			return False
		
		if (not self.pathsOptions.checkIntegrity()):
			return False
		
		
		return True
	


class TaskFileParser:
	
	# main sections
	trainingInformationSection = "training"
	pathsSection = "paths"
	DataAugmentationSection = "data"
	
	
	
	
	
	def _init_():
		pass
	
	
	
	def parseFile(self, taskFilePath: str):
		
		taskInformation = TaskInformation()
		
		to = taskInformation.trainingOptions
		ps = taskInformation.pathsOptions
		
		
		with open(taskFilePath, 'r') as taskFile:
			taskFileParsed = yaml.safe_load(taskFile)
			
			tis = taskFileParsed[self.trainingInformationSection]
			to.modelType = tis['model type']
			to.learningRate = tis['learning rate']
			to.optimizer = tis['optimizer']
			to.savingModelBackupFrequency = tis['saving model backup frequency']
			#for key in tis:
			#	print(f"av key: {key}")
			to.lossFunction = tis["loss function"]
			
			paths = taskFileParsed[self.pathsSection]
			ps.mainDirectory = paths['main directory']
			ps.modelBackupsPattern = paths['model backups pattern']
			
			ps.rawDataPath = paths['raw data path']
			ps.trainingSetPath = paths['training set path']
			ps.validationSetPath = paths['validation set path']
			
			ps.mainModelFile = paths['main model file']
			
			ps.statisticsFile = paths['statistics file']
			ps.metaFile = paths['meta file']
			ps.metaArchiveFile = paths['meta archive file']

			taskInformation.note = taskFileParsed["note"]


			dataAug = taskInformation.dataAugmentationOptions
			dao = taskFileParsed[self.DataAugmentationSection]
			dataAug.datasetVersion = dao["dataset raw version"]
			dataAug.datasetAugmentedDividedVersion = dao["dataset augmented divided version"]
			
			#dataAug.


			
		
		if not taskInformation.checkIntegrity():
			showErrorAndQuit("Couldn't parse the task file correctly.")
		
		return taskInformation
		
	
	
	



























