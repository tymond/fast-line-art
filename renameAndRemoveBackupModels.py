#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#



from utilities import *
import sys
import re

from os import listdir
from os.path import isfile, join, isdir
from pathlib import Path
import os


import argparse


from utilities import *

def shouldRemoveModel(number, leaveEvery) -> bool:
	startingPointMinimum = 10 # good to have at least the first model, or a first few
	if (number < startingPointMinimum):
		return False
	if (number%leaveEvery == 0):
		return False
	return True



def renameByRegex(path, filename, regex, renameTo) -> None:
	patternToReplace = "NNNN"
	if patternToReplace not in renameTo:
		return
	
	match = regex.match(filename)
	#print(f"match = {match}")

	if match is not None and len(match.groups()) >= 1:
		number = (int)(match.group(1))
		newFilename = renameTo.replace(patternToReplace, str(number).zfill(5))
		os.rename(f"{path}/{filename}", f"{path}/{newFilename}")


def canReadModel(fullFilepath, modelType, expectedFormat) -> bool:
	if isfile(fullFilepath):
		model = getModelFromFileOrNew(modelType, fullFilepath, "cpu", expectedFormat)
		if model is None:
			return False
		return True
	return False





def removeByRegex(path: str, filename: str, regex, leaveEvery: int, modelType: str, expectedFormat: str):
	match = regex.match(filename)
	#print(f"match = {match}")
	#print(f"match len = {len(match.groups())}")
	#print(f"m = {(int)(match.group(1))}")
	if (expectedFormat == "pt"):
		print(f"### Expected pt, Model file: {filename} | type {modelType} | format: {expectedFormat} | match = {match} | regex was: {regex}")
	if match is not None and len(match.groups()) >= 1:
		number = (int)(match.group(1))
		#print(f"number = {number}")
		
		fullFilepath = f"{path}/{filename}"

		

		canBeRead = canReadModel(fullFilepath, modelType, expectedFormat)
		if (expectedFormat == "pt"):
			print(f"### Expected pt, Model {filename} can be read: {canBeRead} in format {expectedFormat} therefore should remove it: {shouldRemoveModel(number, leaveEvery)}")
		if not canBeRead:
			return (None, None, None, None) # do nothing, as if the regex didn't fit
		if shouldRemoveModel(number, leaveEvery):
			return (False, number, fullFilepath, filename)
		else:
			return (True, number, fullFilepath, filename)
		
	return (None, None, None, None)



def checkExistingNumbers():
	pass






if __name__ == "__main__":

	print(f"Hello!")

	parser = argparse.ArgumentParser(description='Train in parallel')
	parser.add_argument('-p', '--path', type=str, help="Path to the folder with the models")
	parser.add_argument('-n', '--every', type=int, help="Every nth model will be left alone")
	parser.add_argument('-t', '--type', type=str, help="Model type (for reading)")
	parser.add_argument('-l', '--last', type=int, help="Certainly keep last N models")
	
	
	
	args = parser.parse_args()
	if (args.path is None):
		print(f"Provide a path.")
		quit(0)
	if (args.every is None):
		print(f"Provide a frequency of models. Can be 1.")
		quit(0)
	if (args.type is None):
		print(f"Provide the model type.")
		quit(0)
	if (args.last is None):
		print(f"Provide the model type.")
		quit(0)
	
	
	path = args.path + "/"



	regexCleanBackup = re.compile("model_(\\d+).backup")
	regexOldMlBackup = re.compile("model_(\\d+).backup_ml.ml")
	regexOldPtBackup = re.compile("model_(\\d+).backup_pt.pt")

	regexNewMlBackup = re.compile("model_(\\d+)_backup.ml")
	regexNewPtBackup = re.compile("model_(\\d+)_backup.pt")
	
	
	

	modelsToRemove = []
	modelsToKeep = []
	maxNumber = 0

	# TODO: remove
	#maxNumOfFiles = 50
	#iter = 0

	for item in listdir(path):
		try:
			if canReadModel(f"{path}/{item}", args.type, "ml"): # if it's surely a ml model
				print(f"Now we're gonna rename {item} to model_NNNN_backup.ml if it matches the pattern: {regexCleanBackup}")
				renameByRegex(path, item, regexCleanBackup, "model_NNNN_backup.ml")
		except Exception as e:
			#print(f"Exception: {e}")
			pass

		renameByRegex(path, item, regexOldPtBackup, "model_NNNN_backup.pt")

		tup = removeByRegex(path, item, regexNewMlBackup, args.every, args.type, "ml")
		if tup[0] is None:
			tup = removeByRegex(path, item, regexOldMlBackup, args.every, args.type, "ml")
		if tup[0] is None:
			tup = removeByRegex(path, item, regexOldPtBackup, args.every, args.type, "pt")
		if tup[0] is None:
			tup = removeByRegex(path, item, regexNewPtBackup, args.every, args.type, "pt")
			
		
		
		if tup[0] is None:
			continue

		#print(f"Result here: {tup}")
		maxNumber = max(tup[1], maxNumber)

		if (tup[0]): # means: keep
			modelsToKeep.append(tup)
		else:
			modelsToRemove.append(tup)

		#iter += 1
		#if (iter >= maxNumOfFiles):
		#	break


	for fileEntry in modelsToRemove:
		(flag, number, location, filename) = fileEntry
		if number == maxNumber:
			print(f"Keeping because it's the highest model: {fileEntry}")
			pass
		elif maxNumber - number < args.last:
			#print(f"Keeping because of the LAST option: {fileEntry}")
			pass
		else:
			#print(f"Removing: {fileEntry}")
			Path(location).unlink()