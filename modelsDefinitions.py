#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#


import torch.nn as nn
import torch

# only used for loss functions comparison
def meanSquareLoss(output, target):
	return torch.nn.MSELoss()(output, target)


def blackWhiteLoss(output, target):
	lossMinMultiplier = 0.8
	
	lossOriginal = (output - target)**2
	#loss = lossOriginal # this is mse - mean squared error
	#loss = loss * (torch.abs(target - 0.5))**2 # this ensures that the losses on pure white and pure black are more important
	#loss = loss * (lossMinMultiplier*target**2 - lossMinMultiplier*target + 1)
	#loss = torch.mul(loss, (torch.mul(target**2, lossMinMultiplier) - torch.mul(target, lossMinMultiplier) + 1))
	#loss2 = torch.mul(loss, lossMinMultiplier)
	#print(f"ok loss shape = {loss2.shape}")
	blackWhite1 = torch.mul(target**2, lossMinMultiplier)
	blackWhite2 = -torch.mul(target, lossMinMultiplier)
	blackWhite3 = 1
	blackWhiteFull = torch.add(blackWhite1, blackWhite2)
	blackWhiteFull = torch.add(blackWhiteFull, blackWhite3)

	loss = torch.mul(lossOriginal, blackWhiteFull)
	loss = torch.mean(loss)
	#print(f"bad loss shape = {loss.shape}")
	#loss = torch.mul
	
	return loss

def blackWhiteLossStronger(output, target):
	lossMinMultiplier = 0.3
	
	lossOriginal = (output - target)**2
	blackWhite1 = torch.mul(target**2, lossMinMultiplier)
	blackWhite2 = -torch.mul(target, lossMinMultiplier)
	blackWhite3 = 1
	blackWhiteFull = torch.add(blackWhite1, blackWhite2)
	blackWhiteFull = torch.add(blackWhiteFull, blackWhite3)

	loss = torch.mul(lossOriginal, blackWhiteFull)
	loss = torch.mean(loss)
	
	return loss


def originalModel():
	return nn.Sequential(
		nn.Conv2d(1, 48, kernel_size=(5, 5), stride=(2, 2), padding=(2, 2)),
		nn.ReLU(),
		nn.Conv2d(48, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(512, 1024, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(1024, 1024, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(1024, 1024, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(1024, 1024, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(1024, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(512, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(256, 256, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(128, 128, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 48, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(48, 48, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(48, 24, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(24, 1, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.Sigmoid()
	)


def shallowModel(): # 2 mln of parameters
	return nn.Sequential(
		nn.Conv2d(1, 48, kernel_size=(5, 5), stride=(2, 2), padding=(2, 2)),
		nn.ReLU(),
		nn.Conv2d(48, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(128, 128, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(128, 128, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 48, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(48, 48, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(48, 24, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(24, 1, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.Sigmoid()
	)

def wideAndShallow(): # 9 mln of parameters...
	return nn.Sequential(
		nn.Conv2d(1, 48, kernel_size=(5, 5), stride=(1, 1), padding=(2, 2)),
		nn.ReLU(),
		nn.Conv2d(48, 128, kernel_size=(9, 9), stride=(1, 1), padding=(4, 4)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(5, 5), stride=(1, 1), padding=(2, 2)),
		nn.ReLU(),
		nn.Conv2d(128, 48, kernel_size=(5, 5), stride=(1, 1), padding=(2, 2)),
		nn.ReLU(),
		nn.Conv2d(48, 1, kernel_size=(5, 5), stride=(1, 1), padding=(2, 2)),
		nn.Sigmoid()
		)
	
def wideAndShallowSmall(): #
	return nn.Sequential(
		nn.Conv2d(1, 48, kernel_size=(9, 9), stride=(1, 1), padding=(4, 4)),
		nn.ReLU(),
		nn.Conv2d(48, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 48, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(48, 1, kernel_size=(5, 5), stride=(1, 1), padding=(2, 2)),
		nn.Sigmoid()
		)
	



# originally called "mySuggestion" because it was the first change to the model
def originalSmaller():
	return nn.Sequential(
			nn.Conv2d(1, 48, kernel_size=(5, 5), stride=(2, 2), padding=(2, 2)),
			nn.ReLU(),
			nn.Conv2d(48, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(128, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(256, 512, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(512, 1024, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(1024, 1024, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(1024, 1024, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(1024, 1024, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(1024, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(512, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
			nn.ReLU(),
			nn.ConvTranspose2d(256, 256, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(256, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
			nn.ReLU(),
			nn.ConvTranspose2d(128, 128, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(128, 48, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
			nn.ReLU(),
			nn.ConvTranspose2d(48, 48, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(48, 24, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
			nn.ReLU(),
			nn.Conv2d(24, 1, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
			nn.Sigmoid(),
		)

def tiny():
	return nn.Sequential(
		nn.Conv2d(1, 48, kernel_size=(5, 5), stride=(2, 2), padding=(2, 2)),
		nn.ReLU(),
		nn.Conv2d(48, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(512, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(256, 256, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(128, 128, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 48, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(48, 48, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(48, 24, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(24, 1, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1)),
		nn.Sigmoid(),
	)
	
def tinyTinier():
	return nn.Sequential(
		nn.Conv2d(1, 48, kernel_size=(5, 5), stride=(2, 2), padding=(2, 2)),
		nn.ReLU(),
		nn.Conv2d(48, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(256, 256, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(128, 128, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 48, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(48, 48, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(48, 24, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(24, 1, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1)),
		nn.Sigmoid(),
	)



def tinyNarrower():
	return nn.Sequential(
		nn.Conv2d(1, 48, kernel_size=(5, 5), stride=(2, 2), padding=(2, 2)),
		nn.ReLU(),
		nn.Conv2d(48, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(256, 256, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(128, 64, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(64, 48, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(48, 48, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(48, 24, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(24, 1, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1)),
		nn.Sigmoid(),
	)


def tinyNarrowerShallow():
	return nn.Sequential(
		nn.Conv2d(1, 48, kernel_size=(5, 5), stride=(2, 2), padding=(2, 2)),
		nn.ReLU(),
		nn.Conv2d(48, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(256, 256, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 64, kernel_size=(9, 9), stride=(1, 1), padding=(4, 4)),
		nn.ReLU(),
		nn.ConvTranspose2d(64, 64, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(64, 48, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(48, 48, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(48, 24, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(24, 1, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1)),
		nn.Sigmoid(),
	)




def tooSmallLinear():
	size = 100
	return nn.Sequential(
		#nn.Conv2d(1, 1, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		#nn.ReLU(),
		nn.Flatten(),
		#nn.Unflatten(1, (1, 200*200)),
		#nn.Linear(1, 200*200),
		#nn.Reshape((200*200, 1)),
		#nn.LazyLinear(1, 200*200),
		#nn.LazyLinear(200),
		nn.Linear(size*size, size*size),
		nn.Flatten(),
		nn.Unflatten(1, (1, size, size))
		#nn.ReLU(),
	)

def tooSmallConv():
	return nn.Sequential(
		nn.Conv2d(1, 24, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(24, 96, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(96, 24, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(24, 1, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.Sigmoid(),
		
		#nn.ReLU(),
		#nn.Flatten(),
		#nn.Unflatten(1, (1, 200*200)),
		#nn.Linear(1, 200*200),
		#nn.Reshape((200*200, 1)),
		#nn.LazyLinear(1, 200*200),
		#nn.LazyLinear(200),
		#nn.Linear(size*size, size*size),
		#nn.Flatten(),
		#nn.Unflatten(1, (1, size, size))
		#nn.ReLU(),
	)



def typicalDeep():
	return nn.Sequential(
		nn.Conv2d(1, 48, kernel_size=(5, 5), stride=(1, 1), padding=(2, 2)),
		nn.ReLU(),
		nn.Conv2d(48, 128, kernel_size=(9, 9), stride=(2, 2), padding=(4, 4)), # /2
		nn.ReLU(),
		nn.Conv2d(128, 256, kernel_size=(5, 5), stride=(1, 1), padding=(2, 2)),
		nn.ReLU(),
		nn.Conv2d(256, 256, kernel_size=(5, 5), stride=(1, 1), padding=(2, 2)),
		nn.ReLU(),
		nn.Conv2d(256, 256, kernel_size=(9, 9), stride=(2, 2), padding=(4, 4)),  # /2
		nn.ReLU(),
		nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(512, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(256, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(128, 128, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),  # x2
		nn.ReLU(),
		nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(128, 48, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.ConvTranspose2d(48, 48, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),  # x2
		nn.ReLU(),
		nn.Conv2d(48, 24, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.ReLU(),
		nn.Conv2d(24, 1, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
		nn.Sigmoid()
	)



class CustomModel(torch.nn.Module):
	
	def __init__(self, seq):
		super(CustomModel, self).__init__()
		self.seq = seq

	def forward(self, x):
		x = self.seq(x)
		return x

class SpecialCustomModel(torch.nn.Module):
	def __init__(self, module):
		super(SpecialCustomModel, self).__init__()
		self.module = module

	def forward(self, x):
		x = self.module(x)
		return x
	
	

class TinyModel(torch.nn.Module):

	def __init__(self):
		super(TinyModel, self).__init__()
		self.seq = tooSmallConv()

	def forward(self, x):
		x = self.seq(x)
		return x




def modelTypeToNewModel(modelType: str):
	if modelType == "tooSmallConv":
		return CustomModel(tooSmallConv())
	if modelType == "original":
		return CustomModel(originalModel())
	if modelType == "originalSmaller":
		return CustomModel(originalSmaller())
	if modelType == "tiny":
		return CustomModel(tiny())
	if modelType == "tinyTinier":
		return CustomModel(tinyTinier())
	if modelType == "tinyNarrower":
		return CustomModel(tinyNarrower())
	if modelType == "tinyNarrowerShallow":
		return CustomModel(tinyNarrowerShallow())
	if modelType == "shallowModel":
		return CustomModel(shallowModel())
	if modelType == "wideAndShallow":
		return CustomModel(wideAndShallow())
	if modelType == "wideAndShallowSmall":
		return CustomModel(wideAndShallowSmall())
	if modelType == "typicalDeep":
		return CustomModel(typicalDeep())
	
	
	return None

