#!/usr/bin/python3
#
# This file is part of Krita Fast Line Art Project
#
# SPDX-FileCopyrightText: 2024 Agata Cacko <cacko.azh@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
#

import getopt
import sys

from os.path import isfile

from trainer import *
from utilities import *


def printHelp():
	print(f"Options:")
	print(f"            -h, --help              show this help text")
	print(f"            -m, --modelFile=PATH    location of the model file to use")
	print(f"            -i, --input=PATH        input file path")
	print(f"            -o, --output=PATH       output file path")
	

def getModel(device: str, modelFile: str):
	if (isfile(modelFile)):
		return torch.load(modelFile, map_location=torch.device(device))
	else:
		return None
	

if __name__ == "__main__":
	
	iinput = ""
	output = ""
	modelFile = ""
	invert = ""

	try:
		arguments, values = getopt.getopt(sys.argv[1:], "hi:o:m:v:", ["help", "input=", "output=", "modelFile=", "invert="])
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-h", "--help"):
				printHelp()
				quit()
			
			elif currentArgument in ("-m", "--modelFile"):
				modelFile = currentValue
				
			elif currentArgument in ("-i", "--input"):
				iinput = currentValue
				
			elif currentArgument in ("-o", "--output"):
				output = currentValue
				
			elif currentArgument in ("-v", "--invert"):
				invert = currentValue
				
			
				
	except getopt.error as err:
		showErrorAndQuit(str(err))

	if (not isfile(iinput)):
		showErrorAndQuit(f"File {iinput} specified as input doesn't exist.")
	
	device = getDevice()	
	model = getModel(device, modelFile)
	
	if model == None:
		showErrorAndQuit(f"Can't read model file in the location {modelFile}.")

	eprint(f"Input is: {iinput}, output is: {output}")

	tr = Trainer(model, TaskInformation())
	inputData = tr.prepareData(iinput)
	
	if invert:
		inputData = 1 - inputData
	print(f"input data = {inputData}")
	inputData = inputData.to(device)
	outputData = model(inputData)
	outputData = outputData.to("cpu")
	if invert:
		outputData = 1 - outputData
	tr.saveAsImage(outputData, output)




